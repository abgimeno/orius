/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orius.service;

import euler.bugzilla.beans.query.LegalValueBean;
import euler.bugzilla.j2bugzilla.BugzillaConnector;
import euler.bugzilla.model.ProductInfo;
import euler.bugzilla.model.query.Fields;
import euler.bugzilla.model.query.LegalValue;
import euler.bugzilla.utils.EulerExceptionUtils;
import euler.task.LegalValuesTask;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import orius.OriusContext;

/**
 *
 * @author agime
 */
public class InitialiseLegalValuesService extends Service<Collection<LegalValue>> {

    protected BugzillaConnector conn;
    private final List<ProductInfo> products;
    protected static final Logger log = EulerExceptionUtils.LoggerFactory.make();
    
    public InitialiseLegalValuesService(BugzillaConnector conn, List<ProductInfo> results) {
        this.conn = conn;
        this.products = results;
    }

    @Override
    protected Task<Collection<LegalValue>> createTask() {
        return new LegalValuesTask(conn, products) {

            @Override
            public void persistLegalValue(LegalValue legalValue) {
                try {
                    LegalValueBean lvs = new LegalValueBean(OriusContext.PERSISTENCEUNIT);
                    lvs.persist(legalValue);
                } catch (IOException ex) {
                    log.error(Level.ERROR, ex);
                }
            }

            @Override
            public List<LegalValue> getLegalValues(Fields field, ProductInfo product) {
                try {
                    LegalValueBean lvs = new LegalValueBean(OriusContext.PERSISTENCEUNIT);
                    List results = lvs.getLegalValues(field, product);
                    log.trace(getLogMessage(field, product, results));
                    return results;
                } catch (IOException ex) {
                    log.error(Level.ERROR, ex);
                    return null;
                }
            }

            @Override
            public List<LegalValue> getLegalValues(Fields field) {
                try {
                    LegalValueBean lvs = new LegalValueBean(OriusContext.PERSISTENCEUNIT);
                    List results = lvs.getLegalValues(field);
                    log.trace(getLogMessage(field, null, results));
                    return results;
                } catch (IOException ex) {
                    log.error(Level.ERROR, ex);
                    return null;
                }
            }

            private String getLogMessage(Fields field, ProductInfo prod, List results) {
                StringBuilder sb = new StringBuilder();
                String prodText = (prod == null) ? "" : new StringBuilder(" and product ").append(prod.getName()).toString();

                if (results == null) {
                    sb.append("No legal values have been found for field ");
                    sb.append(field.getInternalName());
                    sb.append(prodText);
                    sb.append(". The results list is null.");
                } else {
                    sb.append(results.size());
                    sb.append(" legal values have been found for field ");
                    sb.append(field.getInternalName());
                    sb.append(prodText);
                    sb.append(" . (");
                    sb.append(results);
                    sb.append(")");
                }
                return sb.toString();
            }
        };
    }
}
