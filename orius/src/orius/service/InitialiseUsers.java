/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orius.service;

import euler.bugzilla.beans.user.UserBean;
import euler.task.UserCollectionTask;
import euler.bugzilla.j2bugzilla.BugzillaConnector;
import euler.bugzilla.model.BugzillaUser;
import euler.bugzilla.utils.EulerExceptionUtils;
import java.util.Collection;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javax.persistence.NoResultException;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import orius.OriusContext;

/**
 *
 * @author agime
 */
public class InitialiseUsers extends Service<Collection<BugzillaUser>> {

    protected final BugzillaConnector conn;
    protected static final Logger log = EulerExceptionUtils.LoggerFactory.make();
    
    public InitialiseUsers(BugzillaConnector conn) {        
        this.conn = conn;
    }

    @Override
    protected Task<Collection<BugzillaUser>> createTask() {
        return new UserCollectionTask(conn) {
            @Override
            public void persistUser(BugzillaUser user) {
                try {
                    
                    UserBean ub = new UserBean(OriusContext.PERSISTENCEUNIT);                    
                    try{                       
                        ub.getUser(user.getEmail());//checks if user exists 
                        log.info("merging " + user.getEmail());
                        ub.merge(user);
                    }catch(NoResultException noRes){
                        log.info("Persisting " + user.getEmail());
                        ub.persist(user);
                    }                    
                } catch (Exception ex) { //do not use specific catch here
                    log.error(Level.ERROR, ex);
                }
            }
        };
    }
              
}
