/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package orius.conf.login;

import euler.bugzilla.beans.BugzillaCredentialsBean;
import euler.bugzilla.dialogfx.DialogFX;
import euler.bugzilla.j2bugzilla.BugzillaConnector;
import euler.bugzilla.j2bugzilla.enums.Action;
import euler.bugzilla.model.ServerInformation;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import orius.AppControllers;
import orius.BaseController;
import orius.OriusContext;

/**
 * FXML Controller class
 *
 * @author agime
 */
public class LoginController extends BaseController implements Initializable {

    @FXML
    private Label errorMessage;

    @FXML
    private Button login;

    @FXML
    private PasswordField password;

    @FXML
    private TextField url;

    @FXML
    private TextField userId;

    private final BugzillaCredentialsBean bcs;

    public LoginController() throws IOException {
        bcs = new BugzillaCredentialsBean(PERSISTENCEUNIT);
    }

    @Override
    public void initialize(URL eurl, ResourceBundle rb) {
        
        assert errorMessage != null : "fx:id=\"errorMessage\" was not injected: check your FXML file 'LoginCredentials.fxml'.";
        assert login != null : "fx:id=\"login\" was not injected: check your FXML file 'LoginCredentials.fxml'.";
        assert password != null : "fx:id=\"password\" was not injected: check your FXML file 'LoginCredentials.fxml'.";
        assert root != null : "fx:id=\"root\" was not injected: check your FXML file.";
        assert url != null : "fx:id=\"url\" was not injected: check your FXML file 'LoginCredentials.fxml'.";
        assert userId != null : "fx:id=\"userId\" was not injected: check your FXML file 'LoginCredentials.fxml'.";
        errorMessage.setText("");
        
        if (parameters != null && parameters.get(Action.class) == Action.MODIFY) {
            serverCredentials = bcs.getCredentials();
            userId.setText(serverCredentials.getUsername());
            password.setText(serverCredentials.getPassword());
            url.setText(serverCredentials.getUrl());
        }
    }

    @FXML
    public void processLogin(ActionEvent event) throws Exception {
        
        if (parameters.get(Action.class) == Action.CREATE) {            
            serverCredentials = new ServerInformation(userId.getText(), password.getText(), url.getText());
            if (loginIsSuccessfull()) {
                bcs.persist(serverCredentials);
                displaySuccessMessage();
                OriusContext.loadScreen(OriusContext.PRODUCTS, (Stage) login.getScene().getWindow(), parameters);
            } else { //loging not successful
                displayErrorDialogue();
            }
        }
        
        if (parameters.get(Action.class) == Action.MODIFY) {
            serverCredentials = (ServerInformation) parameters.get(ServerInformation.class);
            serverCredentials.setUsername(userId.getText());
            serverCredentials.setPassword(password.getText());
            if (serverCredentials.getUrl().equals(url.getText())) {
                serverCredentials.setUrl(url.getText());
                //cleanLegalValues();                    
            }
            if (loginIsSuccessfull()) {
                bcs.merge(serverCredentials);
                displaySuccessMessage();
                root.getScene().getWindow().hide();
            } else { //loging not successful
                displayErrorDialogue();
            }
        }
    }

    @Override
    protected void setController() {
        OriusContext.addController(AppControllers.LOGIN, this);
    }

    @Override
    public void customInitialiser() {
        log.info("Custom initialiser not implemented for this controller.");
    }
    /*
     if user changes server by updating the URL we could clean DB and remove queries
     but for the time being users are advised to delete the database folder
     */

    private void cleanLegalValues() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private boolean loginIsSuccessfull() {
        BugzillaConnector conn = new BugzillaConnector();
        return OriusContext.login(conn, serverCredentials);
    }

    private void displayErrorDialogue() {
        DialogFX dialog = new DialogFX(DialogFX.Type.ERROR);
        dialog.setMessage("Login using the credentials provided was not successful. Please correct the values and try again.");
        dialog.setTitle("Bugzilla server login failed");
        dialog.showDialog();
    }

    private void displaySuccessMessage() {
        DialogFX dialog = new DialogFX(DialogFX.Type.ACCEPT);
        dialog.setMessage("The values provided have been saved successfully.");
        dialog.setTitle("Bugzilla server details saved!");
        dialog.showDialog();
        
    }
}
