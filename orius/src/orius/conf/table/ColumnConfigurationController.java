/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orius.conf.table;

import euler.bugzilla.beans.conf.BugTableBean;
import euler.bugzilla.model.conf.PositionComparator;
import euler.bugzilla.model.conf.QueryTableColumn;
import euler.bugzilla.model.conf.QueryTableConf;
import euler.bugzilla.model.query.Query;
import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Callback;
import np.com.ngopal.listcell.ListCellX;
import orius.AppControllers;
import orius.BaseController;
import orius.OriusContext;

/**
 *
 * FXML Controller class
 *
 * @author agime
 */
public class ColumnConfigurationController extends BaseController implements Initializable {

    private static final String SELECTED = "selected";
    private static final String NOTSELECTED = "notselected";

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button add;

    @FXML
    private ListView<QueryTableColumn> notSelected;

    @FXML
    private Button remove;

    @FXML
    private Button save;

    @FXML
    private ListView<QueryTableColumn> selected;

    private final BugTableBean btb;

    public ColumnConfigurationController() throws IOException {
        btb = new BugTableBean(PERSISTENCEUNIT);
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        super.initialize(url, rb);
        assert add != null : "fx:id=\"add\" was not injected: check your FXML file 'ProductsConfiguration.fxml'.";
        assert notSelected != null : "fx:id=\"notSelected\" was not injected: check your FXML file 'ProductsConfiguration.fxml'.";
        assert remove != null : "fx:id=\"remove\" was not injected: check your FXML file 'ProductsConfiguration.fxml'.";
        assert root != null : "fx:id=\"root\" was not injected: check your FXML file 'ProductsConfiguration.fxml'.";
        assert save != null : "fx:id=\"save\" was not injected: check your FXML file 'ProductsConfiguration.fxml'.";
        assert selected != null : "fx:id=\"selected\" was not injected: check your FXML file 'ProductsConfiguration.fxml'.";
        configureLists();
    }

    @Override
    public void customInitialiser() {
        ObservableList selectedList = FXCollections.<QueryTableColumn>observableArrayList();
        ObservableList notSelectedList = FXCollections.<QueryTableColumn>observableArrayList();
        Map<String, ObservableList<QueryTableColumn>> colConfiguration = initialiseTableColumnList();
        selectedList.setAll(colConfiguration.get(SELECTED));
        notSelectedList.setAll(colConfiguration.get(NOTSELECTED));
        selected.setItems(selectedList);
        notSelected.setItems(notSelectedList);
        add.setGraphic(new ImageView(new Image(ColumnConfigurationController.class.getResourceAsStream("/orius/media/icons/arrow-left-c_15.png"))));
        remove.setGraphic(new ImageView(new Image(ColumnConfigurationController.class.getResourceAsStream("/orius/media/icons/arrow-right-c_15.png"))));
    }

    /*
     This method configures the CellFactory for the two lists displayed
     with the fields selected and the fields not selected.
     */
    private void configureLists() {
        selected.setCellFactory(new Callback<ListView<QueryTableColumn>, ListCell<QueryTableColumn>>() {
            @Override
            public ListCell<QueryTableColumn> call(ListView<QueryTableColumn> p) {
                ListCellX<QueryTableColumn> cell = new ListCellX<QueryTableColumn>() {
                    @Override
                    public void updateItem(QueryTableColumn item, boolean bln) {
                        super.updateItem(item, bln);
                        if (item != null) {
                            setText(item.getName());
                            item.setVisible(true);
                        }
                    }

                };
                cell.init(selected.getItems());
                return cell;
            }
        });
        notSelected.setCellFactory(new Callback<ListView<QueryTableColumn>, ListCell<QueryTableColumn>>() {
            @Override
            public ListCell<QueryTableColumn> call(ListView<QueryTableColumn> p) {
                ListCell<QueryTableColumn> cell = new ListCell<QueryTableColumn>() {
                    @Override
                    public void updateItem(QueryTableColumn item, boolean bln) {
                        super.updateItem(item, bln);
                        if (item != null) {
                            setText(item.getName());
                            item.setVisible(false);
                        }
                    }

                };
                return cell;
            }
        });
        selected.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        notSelected.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    }

    /*
     It adds bug fields to the "Selected" column. There are issues using
     list.remove(item) and list.addAll(item) so the method has to create 
     new arrays and iterate the old ones item by item. 
       It is slow but this is the best solution I could come up with until now
     */
    @FXML
    public void addColumn(ActionEvent event) {

        ObservableList<QueryTableColumn> newSelectedList = FXCollections.<QueryTableColumn>observableArrayList();
        ObservableList<QueryTableColumn> newNotSelectedList = FXCollections.<QueryTableColumn>observableArrayList();
        ObservableList<QueryTableColumn> toMove = notSelected.getSelectionModel().getSelectedItems();
        //log.debug("source: "+event.getSource());
        if (toMove != null && !toMove.isEmpty()) {
            for(QueryTableColumn qtc: selected.getItems()){
                newSelectedList.add(qtc);
            }            
            for (QueryTableColumn qtc : toMove) {
                qtc.setVisible(true);
                btb.merge(qtc);
                newSelectedList.add(qtc);
            }
            for (QueryTableColumn qtc : notSelected.getItems()) {
                if (!toMove.contains(qtc)) {
                    btb.merge(qtc);
                    newNotSelectedList.add(qtc);
                }
            }
            notSelected.setItems(newNotSelectedList);
            selected.setItems(newSelectedList);
        }

    }
    /*
     It removes bug fields from the "Selected" column and adds them to the NotSelected. 
     There are issues using
     list.remove(item) and list.addAll(item) so the method has to create 
     new arrays and iterate the olds item by item. It feels a bit slow but this is the best solution up to now
     */

    @FXML
    public void removeColumn(ActionEvent event) {

        ObservableList<QueryTableColumn> newSelectedList = FXCollections.<QueryTableColumn>observableArrayList();
        ObservableList<QueryTableColumn> newNotSelectedList = FXCollections.<QueryTableColumn>observableArrayList();
        ObservableList<QueryTableColumn> toMove = selected.getSelectionModel().getSelectedItems();

        if (toMove != null && !toMove.isEmpty()) {

            newNotSelectedList.addAll(notSelected.getItems());
            for (QueryTableColumn qtc : toMove) {
                qtc.setVisible(false);
                btb.merge(qtc);
                newNotSelectedList.add(qtc);
            }
            for (QueryTableColumn qtc : selected.getItems()) {
                if (!toMove.contains(qtc)) {
                    btb.merge(qtc);
                    newSelectedList.add(qtc);
                }
            }
            notSelected.setItems(newNotSelectedList);
            selected.setItems(newSelectedList);
        }
    }

    @FXML
    public void saveColumns(ActionEvent event) {
        log.info("Saving columns");
        ObservableList<QueryTableColumn> selectedColumnList = selected.getItems();
        ObservableList<QueryTableColumn> notSelectedcolumnList = notSelected.getItems();
        List<QueryTableColumn> baseList = new LinkedList<>();
        addColumnsToBaseList(baseList, selectedColumnList);
        addColumnsToBaseList(baseList, notSelectedcolumnList);
        QueryTableConf tableConfiguration;
        Query query = (Query) parameters.get(Query.class);
        try {
            tableConfiguration = btb.findConfiguration(query);
            if (tableConfiguration != null) {
                tableConfiguration.setColumns(baseList);
                btb.merge(tableConfiguration);
                log.debug(tableConfiguration.getQuery().getName() + " is being merged.");
            } else {
                log.debug("The system cannot find configuration for " + query.getName());
                tableConfiguration = new QueryTableConf();
                tableConfiguration.setQuery(query);
                tableConfiguration.setColumns(baseList);
                log.debug(tableConfiguration.getQuery().getName() + " is being persisted.");
                btb.persist(tableConfiguration);
            }
        } catch (Exception ex) {
            guiLog.handleException(this.getClass(), ex);
        }
        root.getScene().getWindow().hide();
    }

    @Override
    protected void setController() {
        OriusContext.addController(AppControllers.COLUMNCONF, this);
    }


    /*
     this function returns a Map with 2 values, selected columns and not selected 
     columns list
     */
    private Map<String, ObservableList<QueryTableColumn>> initialiseTableColumnList() {
        Map<String, ObservableList<QueryTableColumn>> columnsMap = new HashMap<>();
        ObservableList<QueryTableColumn> selectedList = FXCollections.<QueryTableColumn>observableArrayList();
        ObservableList<QueryTableColumn> notSelectedList = FXCollections.<QueryTableColumn>observableArrayList();
        try {
            Query query = (Query) parameters.get(Query.class);
            QueryTableConf qtconf = btb.findConfiguration(query);
            if (qtconf != null) {
                List<QueryTableColumn> columns = qtconf.getColumns();
                Collections.sort(columns, new PositionComparator());
                for (QueryTableColumn qtc : columns) {
                    if (qtc.isVisible()) {
                        selectedList.add(qtc);
                    } else {
                        notSelectedList.add(qtc);
                    }
                }
            } else { //TO-DO: after re-factoring default querytableconf is created, the below should not be necessary
                /*log.debug("Could not find a configuration for query " + query.getName());
                for (String columnName : BugInfo.getBugKeys()) {
                    QueryTableColumn column = new QueryTableColumn(columnName);
                    btb.persist(column);
                    notSelectedList.add(column);
                }
                        */
            }
        } catch (NullPointerException ex) {
            log.error("Please check if query has been passed as parameter to the controller.", ex);
        }
        PositionComparator pcom = new PositionComparator();
        SortedList sortedSelected = new SortedList<>(selectedList);
        sortedSelected.setComparator(pcom);
        SortedList sortedNotSelected = new SortedList<>(notSelectedList);
        sortedNotSelected.setComparator(pcom);
        columnsMap.put(SELECTED, sortedSelected);
        columnsMap.put(NOTSELECTED, sortedNotSelected);
        return columnsMap;
    }

    private void addColumnsToBaseList(List<QueryTableColumn> baseList, ObservableList<QueryTableColumn> toBeAdded) {
        int i = baseList.size();
        for(QueryTableColumn qtc:toBeAdded){
            qtc.setPosition(i);
            btb.merge(qtc);
            baseList.add(qtc);
            i++;
        }
    }

}
