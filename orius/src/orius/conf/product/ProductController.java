/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package orius.conf.product;

import euler.bugzilla.beans.ProductBean;
import euler.bugzilla.j2bugzilla.BugzillaConnector;
import euler.bugzilla.j2bugzilla.base.BugzillaException;
import euler.bugzilla.j2bugzilla.base.ConnectionException;
import euler.bugzilla.j2bugzilla.enums.Action;
import euler.bugzilla.j2bugzilla.enums.ProductParams;
import euler.bugzilla.j2bugzilla.rpc.LogIn;
import euler.bugzilla.j2bugzilla.rpc.GetAccessibleProducts;
import euler.bugzilla.model.ProductInfo;
import euler.bugzilla.service.GetAccessibleProductsService;
import orius.service.InitialiseLegalValuesService;
import orius.service.InitialiseUsers;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import javafx.collections.ObservableList;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.apache.log4j.Logger;
import orius.AppControllers;
import orius.BaseController;
import orius.OriusContext;
import euler.fx.table.cell.CheckBoxTableCell;

/**
 *
 * @author agime
 */
public class ProductController extends BaseController implements Initializable {

    @FXML
    private TableView productTable;

    @FXML
    private Button save;

    @FXML
    private ProgressBar pbar;

    private final ProductBean ps;
    
    private GetAccessibleProductsService aps;
    /*
     using booleans legalValuesset and usersSet allows to 
     configure whether to wait for them or not
     setting one as true as default means the app will not wait until executiong finalises
     */
    private Boolean usersSet = true; //set to true does not wait to intialise users
    private Boolean legalValuesSet = false; // set to true does not wait to initialise legal values

    public ProductController() throws IOException {
        ps = new ProductBean(PERSISTENCEUNIT);

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            super.initialize(url, rb);
            assert root != null : "fx:id=\"root\" was not injected: check your FXML file.";
            assert pbar != null : "fx:id=\"pbar\" was not injected: check your FXML file 'ProductsConfiguration.fxml'.";
            assert productTable != null : "fx:id=\"productTable\" was not injected: check your FXML file 'ProductsConfiguration.fxml'.";
            assert save != null : "fx:id=\"save\" was not injected: check your FXML file 'ProductsConfiguration.fxml'.";
            configureTable();
        } catch (Exception ex) {
            guiLog.handleException(this.getClass(), ex);
        }
    }

    @Override
    public void customInitialiser() {
        try {
            log.debug("Running custom intialiser on " + this.getClass().getName());
            getAndUpdateProducts();
        } catch (Exception ex) {
            guiLog.handleException(this.getClass(), ex);
        }
    }

    private void getAndUpdateProducts() {
        Map<ProductParams, List<String>> params = new HashMap<>();
        List<String> includeFields = new ArrayList<>();
        includeFields.add("id");
        includeFields.add("name");
        includeFields.add("description");
        params.put(ProductParams.INCLUDE_FIELDS, includeFields);
        aps = new GetAccessibleProductsService(OriusContext.getBugzillaConnector(serverCredentials), params);
        aps.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
            @Override
            public void handle(WorkerStateEvent t) {
                showTable();
                updateProductTable(((GetAccessibleProductsService) t.getSource()).getValue());                
            }

        });

        aps.start();
        binder.bind(pbar.visibleProperty(), aps.runningProperty());
        binder.bind(pbar.progressProperty(), aps.progressProperty());
        binder.bind(productTable.itemsProperty(), aps.valueProperty());
        
    }

    private void showTable() {
        productTable.setVisible(true);
        save.setVisible(true);
    }

    private void configureTable() {
        configureCheckBoxCell();
        configureProductIdCell();
        configureProductNameCell();
        configureDescriptionCell();
        productTable.setEditable(true);
        productTable.setVisible(false);
        save.setVisible(false);

    }

    private void configureCheckBoxCell() {
        TableColumn<ProductInfo, Boolean> selected = new TableColumn<>();
        selected.setCellValueFactory(new PropertyValueFactory("selected"));
        selected.setCellFactory(new Callback<TableColumn<ProductInfo, Boolean>, TableCell<ProductInfo, Boolean>>() {
            @Override
            public TableCell<ProductInfo, Boolean> call(TableColumn<ProductInfo, Boolean> p) {
                return new CheckBoxTableCell<ProductInfo, Boolean>();                
            }
        });
        productTable.getColumns().add(0, selected);
    }

    private void configureStringColumn(String propertyName, boolean visible) {
        TableColumn<ProductInfo, String> col = new TableColumn<>();
        col.setCellValueFactory(new PropertyValueFactory(propertyName));
        col.setVisible(visible);
        col.setPrefWidth(400);
        productTable.getColumns().add(col);
    }

    private void configureProductIdCell() {
        configureStringColumn("id", false);
    }

    private void configureProductNameCell() {
        configureStringColumn("name", true);
    }

    private void configureDescriptionCell() {
        configureStringColumn("description", true);
    }

    @FXML
    public void saveProducts(ActionEvent event) {
        log.info("Saving products");
        ObservableList<ProductInfo> list = productTable.getItems();
        for (ProductInfo pi : list) {
            ProductInfo dbRecord = ps.find(pi.getId());
            dbRecord.setSelected(pi.getSelected());
            ps.merge(dbRecord);
        }
        Executor executor = Executors.newFixedThreadPool(5);
        final InitialiseLegalValuesService legalValuesService = new InitialiseLegalValuesService(OriusContext.getBugzillaConnector(serverCredentials), ps.getActiveProds());
        final InitialiseUsers usersService = new InitialiseUsers(OriusContext.getBugzillaConnector(serverCredentials));
        legalValuesService.setExecutor(executor);
        usersService.setExecutor(executor);
        legalValuesService.start();
        usersService.start();
        legalValuesService.setOnSucceeded(new EventHandler() {
            @Override
            public void handle(Event t) {
                try {
                    /*
                     using booleans legalValuesset and usersSet allows to 
                     configure whether to wait for them or not
                     setting one as true as default means the app will not wait until executiong finalises
                     (I know it's a bit crappy)*/
                    legalValuesSet = true;
                    checkIfServicesAreDone();
                    binder.bind(pbar.visibleProperty(), usersService.runningProperty());
                    binder.bind(pbar.progressProperty(), usersService.progressProperty());
                } catch (Exception ex) {
                    guiLog.handleException(this.getClass(), ex);
                }
            }
        });
        usersService.setOnSucceeded(new EventHandler() {
            @Override
            public void handle(Event t) {
                usersSet = true;
                checkIfServicesAreDone();
            }
            

        });
        save.setDisable(true);
        productTable.setDisable(true);
        binder.bind(pbar.visibleProperty(), legalValuesService.runningProperty());
        binder.bind(pbar.progressProperty(), legalValuesService.progressProperty());
    }
    private void checkIfServicesAreDone() {
        try {            
            if (usersSet && legalValuesSet) {
                if (parameters.get(Action.class) == Action.MODIFY) {
                    root.getScene().getWindow().hide();
                }
                if (parameters.get(Action.class) == Action.CREATE){ // new approach the below will never be executed
                    OriusContext.loadScreen(OriusContext.MAINSCREEN, (Stage) root.getScene().getWindow());
                }
            }
        } catch (Exception ex) {
            guiLog.handleException(this.getClass(), ex);;
        }
    }
    //CheckBoxTableCell for creating a CheckBox in a table cell


    private void updateProductTable(ObservableList productList) {
        ObservableList<ProductInfo> products = productList;
        for (ProductInfo pi : products) {
            ProductInfo dbRecord = ps.find(pi.getId());
            if (dbRecord == null) {
                log.info("saving " + pi.getName());
                ps.persist(pi);
            }else{
                log.info("merging " + pi.getName());
                dbRecord.setDescription(pi.getDescription());
                dbRecord.setName(pi.getName());
                ps.merge(pi);
            }
        }
    }

    @Override
    protected void setController() {
        OriusContext.addController(AppControllers.PRODUCT, this);
    }


}
