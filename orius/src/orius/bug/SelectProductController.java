/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orius.bug;

import euler.bugzilla.beans.ProductBean;
import euler.bugzilla.j2bugzilla.enums.Action;
import euler.bugzilla.model.ProductInfo;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import orius.AppControllers;
import orius.BaseController;
import orius.OriusContext;
import orius.fx.ProductSelectedEvent;
import orius.fx.cell.RadioButtonTableCell;

/**
 * FXML Controller class
 *
 * @author agime
 */
public class SelectProductController extends BaseController implements Initializable {

    @FXML
    private TableView productTable;

    private final ProductBean ps;
    private ProductInfo pi;

    public SelectProductController() throws IOException {
        ps = new ProductBean(PERSISTENCEUNIT);
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        super.initialize(url, rb);
        assert root != null : "fx:id=\"root\" was not injected: check your FXML file.";
        configureTable();
    }

    private void configureTable() {
        configureCheckBoxCell();
        configureProductIdCell();
        configureProductNameCell();
        configureDescriptionCell();
        productTable.addEventHandler(ProductSelectedEvent.PROD_SELECTED, new EventHandler<ProductSelectedEvent>() {
            @Override
            public void handle(ProductSelectedEvent t) {
                productTable.setDisable(true);
                pi = (ProductInfo) productTable.getItems().get(t.getRow());
                log.debug(pi.getName() + " has been selected");
                gotoAddBug();
            }
        });
        productTable.setItems(ps.getActiveProds());
    }

    private void configureCheckBoxCell() {
        TableColumn<ProductInfo, Boolean> selected = new TableColumn<>();
        selected.setCellValueFactory(new PropertyValueFactory("selected"));
        selected.setCellFactory(new Callback<TableColumn<ProductInfo, Boolean>, TableCell<ProductInfo, Boolean>>() {
            @Override
            public TableCell<ProductInfo, Boolean> call(TableColumn<ProductInfo, Boolean> p) {
                return new RadioButtonTableCell<>();
            }
        });
        productTable.getColumns().add(0, selected);
    }

    private void configureProductIdCell() {
        configureStringColumn("id", false);
    }

    private void configureProductNameCell() {
        configureStringColumn("name", true);
    }

    private void configureDescriptionCell() {
        configureStringColumn("description", true);
    }

    private void configureStringColumn(String propertyName, boolean visible) {
        TableColumn<ProductInfo, String> col = new TableColumn<>();
        col.setCellValueFactory(new PropertyValueFactory(propertyName));
        col.setVisible(visible);
        productTable.getColumns().add(col);
    }

    @Override
    protected void setController() {
        OriusContext.addController(AppControllers.PRODUCT_SELECTOR, this);
    }

    public void gotoAddBug() {
        try {
            Map<Class, Object> params = new HashMap<>();
            params.put(Action.class, Action.CREATE);
            params.put(ProductInfo.class, pi);
            OriusContext.loadScreen(OriusContext.UPDATE_BUG, (Stage) productTable.getScene().getWindow(), params);
        } catch (Exception ex) {
            log.error(Level.ERROR, ex);
        }
    }

    @Override
    public void customInitialiser() {
        log.info("Custom initialiser not implemented for this controller.");
    }
}
