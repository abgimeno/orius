/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orius.bug;


import euler.bugzilla.beans.BugInfoBean;
import euler.bugzilla.beans.ProductBean;
import euler.bugzilla.event.LoadBugCommentsEvent;
import euler.bugzilla.j2bugzilla.BugzillaConnector;
import euler.bugzilla.j2bugzilla.base.BugzillaException;
import euler.bugzilla.j2bugzilla.enums.Action;
import euler.bugzilla.j2bugzilla.rpc.CommentBug;
import euler.bugzilla.j2bugzilla.rpc.ReportBug;
import euler.bugzilla.j2bugzilla.rpc.UpdateBug;
import euler.bugzilla.model.BugInfo;
import euler.bugzilla.model.BugzillaUser;
import euler.bugzilla.model.ProductInfo;
import euler.bugzilla.model.query.Fields;
import euler.bugzilla.utils.EulerExceptionUtils;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import org.apache.log4j.Logger;
import orius.AppControllers;
import orius.BaseController;
import orius.OriusContext;
import euler.bugzilla.utils.LegalValuesUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import javafx.event.Event;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextArea;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.VBox;
import org.apache.commons.lang3.StringUtils;
import orius.event.handler.LoadBugCommentsHandler;
import orius.fx.pane.StatusMessagePane;

/**
 * FXML Controller class
 *
 * @author agime
 */
public class BugUpdateController extends BaseController implements Initializable {

    @FXML
    private TitledPane commentsTiltedPane;
            
    @FXML
    private ComboBox<String> assignedTo;

    @FXML
    private TextField blocks;

    @FXML
    private ListView<String> cc;

    @FXML
    private ComboBox component;

    @FXML
    private TextField deadline;

    @FXML
    private TextField dependsOn;

    @FXML
    private TextArea description;

    @FXML
    private TextField estimatedTime;

    @FXML
    private StatusMessagePane messagePane;

    @FXML
    private ComboBox milestone;

    @FXML
    private ComboBox opSys;

    @FXML
    private ComboBox platform;

    @FXML
    private ComboBox priority;

    @FXML
    private ComboBox product;

    @FXML
    private ComboBox resolution;

    @FXML
    private Label resolutionLabel;

    @FXML
    private ComboBox status;

    @FXML
    private Button saveBug;

    @FXML
    private ComboBox severity;

    @FXML
    private TextField summary;

    @FXML
    private TextField url;

    @FXML
    private ComboBox version;

    private BugInfo bug = null;

    private ProductInfo selectedProduct = null;
    private final ScrollPane commentsPane = new ScrollPane();
    private final ProductBean ps;
    private final BugInfoBean bis;

    public BugUpdateController() throws IOException {
        ps = new ProductBean(PERSISTENCEUNIT);
        bis = new BugInfoBean(PERSISTENCEUNIT);
    }

    @Override
    public void initialize(URL location, ResourceBundle rb) {
        // TODO
        super.initialize(location, rb);
        doAsserts();
        root.addEventHandler(LoadBugCommentsEvent.LOAD_BUGS, new LoadBugCommentsHandler(serverCredentials, commentsPane));
    }

    @FXML
    private void saveBug() {
        try {
            BugzillaConnector conn = OriusContext.getBugzillaConnector(serverCredentials);
            if (parameters.get(Action.class) == Action.CREATE) {
                ReportBug report = new ReportBug(bug);
                //bug.setCC(cc.getSelectionModel().getSelectedItems());
                showCCValues(cc);
                // sets unbinded fields
                bug.setBlocks(getAsList(blocks.getText()));
                bug.setDependsOn(getAsList(dependsOn.getText()));
                conn.executeMethod(report);
                int id = report.getID();
                StringBuilder sb = new StringBuilder("The id of new bug is: ");
                sb.append(id);
                log.debug(sb.toString());
                showSuccessMessage(sb.toString());
            }
            if (parameters.get(Action.class) == Action.MODIFY) {
                bug.setBlocks(getAsList(blocks.getText()));
                bug.setDependsOn(getAsList(dependsOn.getText()));
                showCCValues(cc);
                UpdateBug update = new UpdateBug(bug);
                conn.executeMethod(update);
                log.debug("Execute method called to update bug...");
                if (!description.getText().isEmpty()) {
                    CommentBug newComment = new CommentBug(bug, description.getText());
                    conn.executeMethod(newComment);
                    log.debug("Execute method to add new comment called");
                    StringBuilder sb = new StringBuilder("Bug ");
                    sb.append(bug.getId());
                    sb.append(" has been updated");
                    showSuccessMessage(sb.toString());
                }

            }
            root.getScene().getWindow().hide();
        } catch (BugzillaException ex) {
            guiLog.handleException(this.getClass(), ex);
        }
    }

    private void doAsserts() {
        assert assignedTo != null : "fx:id=\"assignedTo\" was not injected: check your FXML file 'BugUpdate.fxml'.";
        assert blocks != null : "fx:id=\"blocks\" was not injected: check your FXML file 'BugUpdate.fxml'.";
        assert cc != null : "fx:id=\"cc\" was not injected: check your FXML file 'BugUpdate.fxml'.";
        assert commentsTiltedPane != null : "fx:id=\"commentsTiltedPane\" was not injected: check your FXML file 'BugUpdate.fxml'."; 
        assert component != null : "fx:id=\"component\" was not injected: check your FXML file 'BugUpdate.fxml'.";
        assert deadline != null : "fx:id=\"deadline\" was not injected: check your FXML file 'BugUpdate.fxml'.";
        assert dependsOn != null : "fx:id=\"dependsOn\" was not injected: check your FXML file 'BugUpdate.fxml'.";
        assert description != null : "fx:id=\"description\" was not injected: check your FXML file 'BugUpdate.fxml'.";
        assert estimatedTime != null : "fx:id=\"estimatedTime\" was not injected: check your FXML file 'BugUpdate.fxml'.";
        assert messagePane != null : "fx:id=\"messagePane\" was not injected: check your FXML file 'BugUpdate.fxml'.";
        assert milestone != null : "fx:id=\"milestone\" was not injected: check your FXML file 'BugUpdate.fxml'.";
        assert opSys != null : "fx:id=\"opSys\" was not injected: check your FXML file 'BugUpdate.fxml'.";
        assert platform != null : "fx:id=\"platform\" was not injected: check your FXML file 'BugUpdate.fxml'.";
        assert priority != null : "fx:id=\"priority\" was not injected: check your FXML file 'BugUpdate.fxml'.";
        assert product != null : "fx:id=\"product\" was not injected: check your FXML file 'BugUpdate.fxml'.";
        assert resolution != null : "fx:id=\"resolution\" was not injected: check your FXML file 'BugUpdate.fxml'.";
        assert resolutionLabel != null : "fx:id=\"resolutionLabel\" was not injected: check your FXML file 'BugUpdate.fxml'.";
        assert root != null : "fx:id=\"root\" was not injected: check your FXML file 'BugUpdate.fxml'.";
        assert saveBug != null : "fx:id=\"saveBug\" was not injected: check your FXML file 'BugUpdate.fxml'.";
        assert severity != null : "fx:id=\"severity\" was not injected: check your FXML file 'BugUpdate.fxml'.";
        assert status != null : "fx:id=\"status\" was not injected: check your FXML file 'BugUpdate.fxml'.";
        assert summary != null : "fx:id=\"summary\" was not injected: check your FXML file 'BugUpdate.fxml'.";
        assert url != null : "fx:id=\"url\" was not injected: check your FXML file 'BugUpdate.fxml'.";
        assert version != null : "fx:id=\"version\" was not injected: check your FXML file 'BugUpdate.fxml'.";

    }

    private void doBindings() {
        binder.bind(bug.assignedToProperty(), assignedTo.valueProperty());

        //value is not binded for blocks, please see explanation further below
        //binder.bind(bug.blocksProperty(), blocks.textProperty());
        binder.bind(bug.componentProperty(), component.valueProperty());
        binder.bind(bug.deadlineProperty(), deadline.textProperty());
        /*
        value is not binded, depends is a list property and want to show this as text field
        probably TextField can be extended to support listproperty
        binder.bind(bug.dependsOnProperty(), dependsOn.textProperty());
        */
        binder.bind(bug.descriptionProperty(), description.textProperty());
        binder.bind(bug.estimatedTimeProperty(), estimatedTime.textProperty());
        binder.bind(bug.opsysProperty(), opSys.valueProperty());
        binder.bind(bug.platformProperty(), platform.valueProperty());
        binder.bind(bug.productProperty(), product.valueProperty());
        binder.bind(bug.priorityProperty(), priority.valueProperty());
        binder.bind(bug.resolutionProperty(), resolution.valueProperty());
        binder.bind(bug.bugSeverityProperty(), severity.valueProperty());
        binder.bind(bug.bugStatusProperty(), status.valueProperty());        
        binder.bind(bug.summaryProperty(), summary.textProperty());
        binder.bind(bug.versionProperty(), version.getSelectionModel().selectedItemProperty());
        binder.bind(bug.urlProperty(), url.textProperty());
    }

    private void doPopulateLists() {
        try {
            cc.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
            List<String> userList = getNames(LegalValuesUtils.getUserList(PERSISTENCEUNIT, serverCredentials));
            product.setDisable(true);
            binder.bind(component.itemsProperty(), LegalValuesUtils.getItemsForField(PERSISTENCEUNIT, Fields.COMPONENT, selectedProduct));
            binder.bind(platform.itemsProperty(), LegalValuesUtils.getItemsForField(PERSISTENCEUNIT, Fields.REP_PLATFORM));
            binder.bind(opSys.itemsProperty(), LegalValuesUtils.getItemsForField(PERSISTENCEUNIT, Fields.OP_SYS));
            binder.bind(priority.itemsProperty(), LegalValuesUtils.getItemsForField(PERSISTENCEUNIT, Fields.PRIORITY));
            binder.bind(resolution.itemsProperty(), LegalValuesUtils.getItemsForField(PERSISTENCEUNIT, Fields.RESOLUTION));
            binder.bind(status.itemsProperty(), LegalValuesUtils.getItemsForField(PERSISTENCEUNIT, Fields.STATUS));
            binder.bind(severity.itemsProperty(), LegalValuesUtils.getItemsForField(PERSISTENCEUNIT, Fields.SEVERITY));
            binder.bind(version.itemsProperty(), LegalValuesUtils.getItemsForField(PERSISTENCEUNIT, Fields.VERSION, selectedProduct));
            binder.bind(milestone.itemsProperty(), LegalValuesUtils.getItemsForField(PERSISTENCEUNIT, Fields.MILESTONE, selectedProduct));
            assignedTo.getItems().addAll(userList);

        } catch (BugzillaException ex) {
            guiLog.handleException(this.getClass(), ex);
        }
    }


    /*
     * this function is called before void initialize(URL location, ResourceBundle rb) above, this is possible 
     * due to the approach followed to load FXML through OriusContext
     * obviusly the below has to be dynamic    
     */
    private void doInitialiseDefaultValues() {
        if (parameters.get(Action.class) == Action.CREATE) {
            product.getSelectionModel().select(selectedProduct.getName());
            component.getSelectionModel().select(component.getItems().get(0));
            milestone.getSelectionModel().select(milestone.getItems().get(0));
            opSys.getSelectionModel().select(opSys.getItems().get(0));
            platform.getSelectionModel().select(platform.getItems().get(0));
            priority.getSelectionModel().select(priority.getItems().get(0));
            status.getSelectionModel().select(status.getItems().get(status.getItems().indexOf("NEW")));
            severity.getSelectionModel().select(severity.getItems().get(0));
            version.getSelectionModel().select(version.getItems().get(0));
        }
        if (parameters.get(Action.class) == Action.MODIFY) {
            product.getSelectionModel().select(bug.getProduct());
            component.getSelectionModel().select(bug.getComponent());
            milestone.getSelectionModel().select(bug.getMilestone());
            opSys.getSelectionModel().select(bug.getOpsys());
            platform.getSelectionModel().select(bug.getPlatform());
            priority.getSelectionModel().select(bug.getPriority());
            status.getSelectionModel().select(bug.getStatus());
            severity.getSelectionModel().select(bug.getSeverity());
            version.getSelectionModel().select(bug.getVersion());
            resolution.getSelectionModel().select(bug.getResolution());
            summary.setText(bug.getSummary());
            blocks.setText(bug.getBlocks().toString());
            assignedTo.getSelectionModel().select(bug.getAssignedTo());
            estimatedTime.setText(bug.getEstimatedTime());
            blocks.setText(StringUtils.join(getStringList(bug.getBlocks()), ","));
            dependsOn.setText(StringUtils.join(getStringList(bug.getDependsOn()), ","));
            deadline.setText(bug.getDeadline());            
        }
    }

    @Override
    protected void setController() {
        OriusContext.addController(AppControllers.UPDATE_BUG, this);
    }

    @Override
    public void setParameters(Map parameters) {
        super.setParameters(parameters); //To change body of generated methods, choose Tools | Templates.

    }

    private List<String> getNames(List<BugzillaUser> userList) {
        List<String> names = new ArrayList();
        for (BugzillaUser user : userList) {
            names.add(user.getName());
        }
        return names;
    }

    @Override
    public void customInitialiser() {
        //this controller expects to have a product in the parameters, already selected using SelectProduct controller

        if (parameters.get(Action.class) == Action.CREATE) {
            bug = new BugInfo();
            selectedProduct = (ProductInfo) parameters.get(ProductInfo.class);
            resolution.setVisible(false);
            resolutionLabel.setVisible(false);
            doBindings();
            doPopulateLists();
            doInitialiseDefaultValues();

        }
        if (parameters.get(Action.class) == Action.MODIFY) {
            if (parameters.containsKey(BugInfo.class)) {                
                bug = bis.find(((BugInfo) parameters.get(BugInfo.class)).getBugId());
                LoadBugCommentsEvent lbce = new LoadBugCommentsEvent(Integer.parseInt(bug.getId()), LoadBugCommentsEvent.LOAD_BUGS);
                Event.fireEvent(root, lbce);                  
                selectedProduct = ps.getProduct(bug.getProduct());
                // The order in which the below actions are executed is important
                // bining needs to be performed after values are populated in the list
                doPopulateLists();
                doInitialiseDefaultValues();
                doBindings();
                commentsTiltedPane.setContent(commentsPane);
            }
        }
    }

    private void showSuccessMessage(String message) {
        messagePane.setStyle(CSS_SUCCESS);
        messagePane.setMessageText(message);
        messagePane.displayPane();
    }
    
    /*
    bugs dependent and blocking other bugs ("depend_on" and "blocks" fields are
    displayed as a comma separated string but stored in DB as List of values
    this funcion returns a list of integer (to be stored on db) from a list of 
    comma separated values
    */
    private List<Integer> getAsList(String text) {
        List<String> strings = Arrays.asList(text.split("\\s*,\\s*"));
        List<Integer> integers = new ArrayList<>();
        for (String s : strings) {
            if (!s.isEmpty()) {
                try {
                    integers.add(Integer.parseInt(s));
                } catch (NumberFormatException ex) {
                    log.debug(s+" has been ignored");
                }
            }
        }
        return integers;
    }
    /*
    bugs dependent and blocking other bugs ("depend_on" and "blocks" fields are
    displayed as a comma separated string but stored in DB as List of values
    this funcion returns comma separated values from a list of integer (retrieved from db)
   the value returned is passed to StringUtils.join to get a comma separated string of the list
    */
    private Collection getStringList(List<Integer> integers) {
        List<String> strings = new ArrayList<>();
        for(Integer i: integers){
            strings.add(Integer.toString(i));
        }
        return strings;
    }

    private void showCCValues(ListView<String> cc) {
        log.debug("Emails selected on Cc");
        StringBuilder sb = new StringBuilder("***");
        for (String s : cc.getSelectionModel().getSelectedItems()) {
            sb.append(s);
            sb.append(";");
        }
        
        log.debug(sb.toString());
    }

}
