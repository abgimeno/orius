/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package orius.query;

import euler.bugzilla.model.query.Fields;
import euler.bugzilla.model.query.SearchLimiter;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;

/**
 *
 * @author agime
 */
public class SearchParameterListView extends AbstractSearchParameter implements SearchParameterNode<ListView> {

    private SearchLimiter param;
    private ListView parameters;
    private Label label;
    
        
    public SearchParameterListView(Fields field, ListView parameters) {
        super();
        this.param = mapping.get(field);
        this.parameters = parameters;
        this.label = new Label(field.getInternalName());
        getChildren().addAll(label, parameters);
    }

    public SearchParameterListView(SearchLimiter lim, ListView parameters) {
        super();
        this.param = lim;
        this.parameters = parameters;
        this.label = new Label(param.getName());
        getChildren().addAll(label, parameters);
    }

    @Override
    public ListView getParameters() {
        return parameters;
    }

    @Override
    public ObservableList getSelectedParameters() {
        return parameters.getSelectionModel().getSelectedItems();
    }

    @Override
    public void setParameters(ListView parameters) {
        this.parameters = parameters;
    }

    @Override
    public Label getLabel() {
        return label;
    }

    @Override
    public void setLabel(Label label) {
        this.label = label;
    }

    @Override
    public SearchLimiter getParam() {
        return param;
    }

    @Override
    public void setParam(SearchLimiter param) {
        this.param = param;
    }   

    
}
