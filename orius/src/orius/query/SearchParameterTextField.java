/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package orius.query;

import euler.bugzilla.model.query.Fields;
import euler.bugzilla.model.query.SearchLimiter;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 *
 * @author agime
 */
public class SearchParameterTextField extends AbstractSearchParameter implements SearchParameterNode<TextField>{
    private SearchLimiter param;
    private TextField parameter;
    private Label label;
    
        
    public SearchParameterTextField(Fields field, TextField textField) {
        super();
        this.param = mapping.get(field);
        this.parameter = textField;
        this.label = new Label(field.getInternalName());
        getChildren().addAll(label, parameter);
    }

    public SearchParameterTextField(SearchLimiter lim, TextField textField) {
        super();
        this.param = lim;
        this.parameter = textField;
        this.label = new Label(param.getName());
        getChildren().addAll(label, parameter);
    }

    @Override
    public TextField getParameters() {
        return parameter;
    }

    @Override
    public ObservableList getSelectedParameters() {
        ObservableList<String> items = new SimpleListProperty(FXCollections.<String>observableArrayList());
        if (!parameter.getText().isEmpty()) {
            items.add(parameter.getText());
        }
        return items;
    }

    @Override
    public void setParameters(TextField parameter) {
        this.parameter = parameter;
    }

    @Override
    public Label getLabel() {
        return label;
    }
    
    public void setLabelText(String text) {
        this.label.setText(text);
    }

    @Override
    public SearchLimiter getParam() {
        return param;
    }

    @Override
    public void setParam(SearchLimiter param) {
        this.param = param;
    }

    @Override
    public void setLabel(Label label) {
        this.label=label;
    }
    
}
