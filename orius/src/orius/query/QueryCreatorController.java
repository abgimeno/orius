/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package orius.query;

import euler.bugzilla.beans.BaseBean;
import euler.bugzilla.beans.BugInfoBean;
import euler.bugzilla.beans.ProductBean;
import euler.bugzilla.beans.query.LegalValueBean;
import euler.bugzilla.beans.query.QueryBean;
import euler.bugzilla.exception.LegalValuesMissingException;
import euler.bugzilla.j2bugzilla.BugzillaConnector;
import euler.bugzilla.j2bugzilla.base.BugzillaException;
import euler.bugzilla.j2bugzilla.base.Product;
import euler.bugzilla.j2bugzilla.enums.UserInfoParams;
import euler.bugzilla.j2bugzilla.rpc.LogIn;
import euler.bugzilla.j2bugzilla.user.UserInfo;
import euler.bugzilla.model.BugzillaUser;
import euler.bugzilla.model.ProductInfo;
import euler.bugzilla.model.query.Fields;
import euler.bugzilla.model.query.Query;
import euler.bugzilla.model.query.QueryFolder;
import euler.bugzilla.model.query.QueryParameters;
import euler.bugzilla.model.query.SearchLimiter;
import euler.bugzilla.service.J2BugSearchService;
import euler.bugzilla.utils.LegalValuesUtils;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import orius.BaseController;
import javafx.scene.control.TabPane;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import orius.AppControllers;
import orius.MainWindowController;
import orius.OriusContext;
import orius.fx.pane.StatusMessagePane;

/**
 * FXML Controller class
 *
 * @author agime
 */
public class QueryCreatorController extends BaseController implements Initializable {

    @FXML
    private Button execute;

    @FXML
    private Button save;

    @FXML
    private HBox summaryBox;

    @FXML
    private HBox mainParameters;
    
    @FXML
    private HBox rejectionBox;
    
    @FXML
    private HBox secondaryParams;

    @FXML
    private HBox byPeople;

    @FXML
    private VBox vboxContainer;

    @FXML //  fx:id="name"
    private TextField name; // Value injected by FXMLLoader

    @FXML //  fx:id="queryResultsTab"
    private TabPane queryResultsTab; // Value injected by FXMLLoader

    private TableView results;

    private final ListView prodList = createEmtyListView(LISTWIDTH, LISTHEIGHT, SelectionMode.MULTIPLE);

    private final ProductBean ps;
    private final BaseBean bs;
    private final BugInfoBean bis;
    private final QueryBean qs;
    private final LegalValueBean lvs;

    private final Logger logger = LogManager.getLogger(this.getClass().getName());

    private QueryFolder parent;

    private static final int LISTWIDTH = 127;
    private static final int LISTHEIGHT = 200;

    private final List<Product> storedProd;
    private final List<String> prodNames;
    
    private final Map<SearchLimiter, List<String>> queryParams = new HashMap<>();
    private final Map<SearchLimiter, List<String>> rejectionParams = new HashMap<>();
    
    private J2BugSearchService service;

    public QueryCreatorController() throws IOException {
        this.ps = new ProductBean(PERSISTENCEUNIT);
        this.bs = new BaseBean(PERSISTENCEUNIT);
        this.bis = new BugInfoBean(PERSISTENCEUNIT);
        this.qs = new QueryBean(PERSISTENCEUNIT);
        this.lvs = new LegalValueBean(PERSISTENCEUNIT);
        storedProd = ps.getActiveProductList();
        prodNames = getProductNames(storedProd); // I need a list of String to pass to the service
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        super.initialize(url, rb);
        assert execute != null : "fx:id=\"execute\" was not injected: check your FXML file 'QueryCreator.fxml'.";
        assert name != null : "fx:id=\"name\" was not injected: check your FXML file 'QueryCreator.fxml'.";
        assert queryResultsTab != null : "fx:id=\"queryResultsTab\" was not injected: check your FXML file 'QueryCreator.fxml'.";
        assert vboxContainer != null : "fx:id=\"vboxContainer\" was not injected: check your FXML file 'QueryCreator.fxml'.";
        assert mainParameters != null : "fx:id=\"mainParameters\" was not injected: check your FXML file 'QueryCreator.fxml'.";
        assert results != null : "fx:id=\"results\" was not injected: check your FXML file 'QueryCreator.fxml'.";
        assert root != null : "fx:id=\"root\" was not injected: check your FXML file.";
        assert save != null : "fx:id=\"save\" was not injected: check your FXML file 'QueryCreator.fxml'.";
        assert secondaryParams != null : "fx:id=\"secondaryParams\" was not injected: check your FXML file 'QueryCreator.fxml'.";
        assert summaryBox != null : "fx:id=\"summaryBox\" was not injected: check your FXML file 'QueryCreator.fxml'.";
    }

    @Override
    public void customInitialiser() {
        try {
            createSummaryPanel();
            createSearchParametersPanel();
            createByPeoplePanel();
            results = director.createTable();
        } catch (Exception ex) {
            logger.error(ex);
        }
    }

    private void createSummaryPanel() {
        SearchParameterTextField sptx = new SearchParameterTextField(SearchLimiter.SUMMARY, new TextField());
        summaryBox.getChildren().add(sptx);
        SearchParameterTextField rejection = new SearchParameterTextField(SearchLimiter.SUMMARY, new TextField());
        rejection.setLabelText("- "+SearchLimiter.SUMMARY.getName());
        rejectionBox.getChildren().add(rejection);      
    }

    private void createSearchParametersPanel() {
        createProductsList();
        for (final Fields field : Fields.values()) {
            final ListView lv = createEmtyListView(LISTWIDTH, LISTHEIGHT, SelectionMode.MULTIPLE);
            if (!areLegalValuesStoredOnDB(field)) {
                try {
                    throw new LegalValuesMissingException("No legal values found on DB, please run configuration script");
                } catch (LegalValuesMissingException ex) {
                    guiLog.handleException(this.getClass(), ex);
                }
            } else {
                log.info("Retrieving stored legal values...");
                ObservableList<String> items = new SimpleListProperty(FXCollections.<String>observableArrayList());
                items.addAll(lvs.getLegalValues(field));
                lv.setItems(items);
            }
            SearchParameterListView sp = new SearchParameterListView(field, lv);
            addFieldToPanel(field, sp);
        }
    }

    private boolean areLegalValuesStoredOnDB(Fields field) {
        final List<String> legalValues = lvs.getLegalValues(field);
        return !(legalValues == null || legalValues.isEmpty());
    }

    private void createProductsList() {
        prodList.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            //upon selecting products the filtered fields (version, components, etc.) is also filtered
            @Override
            public void changed(ObservableValue ov, Object oldValue, Object newValue) {
                log.debug("ov: " + ov);
                log.debug("oldVAlue: " + oldValue + " newValue: " + newValue);
                for (Node n : secondaryParams.getChildren()) {
                    SearchParameterListView slv = (SearchParameterListView) n;
                    if (LegalValuesUtils.filterResults(slv.getParam())) {
                        ObservableList<String> data = FXCollections.observableArrayList();
                        List<ProductInfo> products = ps.getProductList(prodList.getSelectionModel().getSelectedItems());
                        data.addAll(lvs.getLegalValues(slv.getField(slv.getParam()), products));
                        slv.getParameters().setItems(data);
                    }
                }
            }

        });
        prodList.getItems().addAll(prodNames);
        SearchParameterListView prodListView = new SearchParameterListView(SearchLimiter.PRODUCT, prodList);
        mainParameters.getChildren().add(prodListView);
    }

    private void createByPeoplePanel() {
        try {
            TextField box = new TextField();

            SearchParameterTextField reporter = new SearchParameterTextField(SearchLimiter.REPORTER, box);
            box = new TextField();

            SearchParameterTextField owner = new SearchParameterTextField(SearchLimiter.OWNER, box);
            byPeople.getChildren().addAll(owner, reporter);
        } catch (Exception ex) {
            guiLog.handleException(this.getClass(), ex);
        }
    }

    @FXML
    public void executeQuery(ActionEvent event) {
        try {
            prepareMapWithParameters();
            service = new J2BugSearchService(OriusContext.getBugzillaConnector(serverCredentials), queryParams);
            service.setRejections(rejectionParams);
            for (SearchLimiter sl : queryParams.keySet()) {
                log.trace(sl.getName() + queryParams.get(sl));
            }
            results.itemsProperty().bind(service.valueProperty());
            execute.setGraphic(createProgIndicatorForButton(service));
            service.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
                @Override
                public void handle(WorkerStateEvent t) {
                    int nresults = service.getValue().size();
                    showResultsMessage(nresults);
                    execute.setGraphic(null);
                }

            });
            service.start();
        } catch (Exception ex) {
            guiLog.handleException(this.getClass(), ex);
        }
    }

    @FXML
    public void saveQuery(ActionEvent event) {
        try {
            if (name.getText().isEmpty()) {
                showMessage(CSS_ERROR, "In order to save your query please provide first a name for the query. Please provide a name and try again.");
            } else {
                final Query newQuery = new Query();
                newQuery.setName(name.getText());
                prepareMapWithParameters();
                newQuery.setParams(persistQueryParams());
                newQuery.setFolder(parent);
                qs.persist(newQuery);
                service = new J2BugSearchService(OriusContext.getBugzillaConnector(serverCredentials), queryParams);
                service.setRejections(rejectionParams);
                save.setGraphic(createProgIndicatorForButton(service));
                service.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
                    @Override
                    public void handle(WorkerStateEvent t) {
                        int nresults = service.getValue().size();
                        newQuery.setBugs(qs.persistBugs(results.getItems()));
                        newQuery.setNbugs(nresults);
                        bs.merge(newQuery);
                        MainWindowController mwc = (MainWindowController) OriusContext.getController(AppControllers.MAINWINDOW);
                        mwc.addQueryToTree(newQuery, parent);
                        showSuccessSaveMessage(nresults, newQuery);
                        save.setGraphic(null);
                    }

                });
                results.itemsProperty().bind(service.valueProperty());
                service.start();
            }
        } catch (Exception ex) {
            guiLog.handleException(this.getClass(), ex);
        }
    }

    private void showSuccessSaveMessage(int nresults, Query newQuery) {
        StringBuilder sb = new StringBuilder(newQuery.getName());
        sb.append(" has been saved successfuly on tree. ");
        sb.append(nresults);
        sb.append(" bugs were found matching selected parameters.");
        showMessage(CSS_SUCCESS, sb.toString());
    }

    private void showResultsMessage(Integer nresults) {
        StringBuilder sb = new StringBuilder();
        sb.append(nresults);
        sb.append(" bugs were found matching selected parameters.");
        showMessage(CSS_SUCCESS, sb.toString());
    }
    /*
     * Bugzilla API only works with List<String> hence this function
     * (and also is easier when we use ListView objects)     
     * creates a list of strings from list of products set by the user
     * and stored in DB
     * */

    private List<String> getProductNames(List<Product> products) {
        List<String> names = new ArrayList<>();
        for (Product p : products) {
            names.add(p.getName());
        }
        return names;
    }

    public List<String> getUserList() throws BugzillaException {
        List<String> names = new ArrayList<>();
        try {
            /*The below code retrieves list of all users for
             * autocompleting search field, but with lot of users 
             * like on mozilla.org there are performance issues
             * 
             * */
             LogIn logIn = new LogIn(serverCredentials.getUsername(), serverCredentials.getPassword());
             BugzillaConnector conn = new BugzillaConnector();
             conn.connectTo(serverCredentials.getUrl());
             conn.executeMethod(logIn);
             Map<UserInfoParams, List<String>> parameters = new HashMap<>();
             List<String> keys = new ArrayList<>();
             keys.add("*");
             parameters.put(UserInfoParams.MATCH, keys);
             UserInfo method = new UserInfo(parameters);
             conn.executeMethod(method);
             for (BugzillaUser bu : method.getResults()) {
                names.add(bu.getEmail());
             }

        } catch (Exception ex) {
            guiLog.handleException(this.getClass(), ex);
        }
        return names;
    }

    private List<QueryParameters> persistQueryParams() {
        List<QueryParameters> params = new ArrayList<>();
        for (SearchLimiter sl : queryParams.keySet()) {
            QueryParameters qp = new QueryParameters(sl, queryParams.get(sl));
            bs.persist(qp);
            params.add(qp);
        }
        return params;

    }

    public QueryFolder getParent() {
        return parent;
    }

    public void setParentFolder(QueryFolder parent) {
        this.parent = parent;
    }

    @Override
    protected void setController() {
        OriusContext.addController(AppControllers.QUERYCREATOR, this);
    }

    private ListView createEmtyListView(int width, int height, SelectionMode mode) {
        ListView lv = new ListView();
        lv.setPrefHeight(height);
        lv.setPrefWidth(width);
        lv.getSelectionModel().setSelectionMode(mode);
        return lv;
    }

    private void prepareMapWithParameters() {
        queryParams.clear();
        rejectionParams.clear();
        prepareMap(summaryBox, queryParams);
        prepareMap(mainParameters,queryParams);
        prepareMap(secondaryParams, queryParams);
        prepareMap(byPeople, queryParams);
        prepareMap(rejectionBox, rejectionParams);

    }


    /*
     * It expects an HBox populated with
     * SearchParameterNode implementations
     * Prepares the Map object to pass to the webservice
     */
    private void prepareMap(HBox parameters, Map<SearchLimiter, List<String>> map) {
        for (Node n : parameters.getChildren()) {
            SearchParameterNode sp = (SearchParameterNode) n;
            ObservableList<String> list = sp.getSelectedParameters();
            if (!list.isEmpty()) {
                map.put(sp.getParam(), list);
            } else { // when user does not select any product,the initially set products are used as filter
                if (sp.getParam() == SearchLimiter.PRODUCT) {
                    map.put(SearchLimiter.PRODUCT, prodNames);
                }
            }
        }
    }

    private void addFieldToPanel(Fields field, SearchParameterListView sp) {
        if (!isSecondary(field)) {// Secondary fields are those displayed in the collapsible panel
            mainParameters.getChildren().add(sp);
        } else {
            secondaryParams.getChildren().add(sp);
        }
    }

    private void showMessage(String style, String message) {
        StatusMessagePane messagePane = new StatusMessagePane();
        messagePane.setStyle(style);
        messagePane.setMessageText(message);
        messagePane.displayPane();
        vboxContainer.getChildren().add(1, messagePane);
        messagePane.getCloseButton().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                vboxContainer.getChildren().remove(1);
            }
        });
        log.trace(message);
    }

    private Node createProgIndicatorForButton(J2BugSearchService service) {
        ProgressIndicator pi = new ProgressIndicator();
        pi.setPrefWidth(5);
        pi.setPrefHeight(5);
        pi.visibleProperty().bind(service.runningProperty());
        pi.progressProperty().bind(service.progressProperty());
        return pi;
    }

}
