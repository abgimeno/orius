/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package orius.query;

import euler.bugzilla.model.query.Fields;
import euler.bugzilla.model.query.SearchLimiter;
import javafx.collections.ObservableList;
import javafx.scene.control.Control;
import javafx.scene.control.Label;


/**
 *
 * @author agime
 */
public interface SearchParameterNode<T extends Control>{
           
    public Label getLabel();
    public SearchLimiter getParam();
    public Control getParameters();
    public ObservableList getSelectedParameters();
    public Fields getField(SearchLimiter slimiter);
    public void setLabel(Label label);
    public void setParam(SearchLimiter param);
    public void setParameters(T parameters);
    
}
