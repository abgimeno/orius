/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orius.query;

import euler.bugzilla.model.query.Fields;
import euler.bugzilla.model.query.SearchLimiter;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import javafx.scene.layout.VBox;

/**
 *
 * @author agime
 */
public abstract class AbstractSearchParameter extends VBox {

    protected static Map<Fields, SearchLimiter> mapping = new HashMap<>();

    static {
        Map<Fields, SearchLimiter> map = new HashMap<>();
        map.put(Fields.COMPONENT, SearchLimiter.COMPONENT);
        map.put(Fields.OP_SYS, SearchLimiter.OPERATING_SYSTEM);
        map.put(Fields.PRIORITY, SearchLimiter.PRIORITY);
        map.put(Fields.REP_PLATFORM, SearchLimiter.PLATFORM);
        map.put(Fields.RESOLUTION, SearchLimiter.RESOLUTION);
        map.put(Fields.SEVERITY, SearchLimiter.SEVERITY);
        map.put(Fields.STATUS, SearchLimiter.STATUS);
        map.put(Fields.VERSION, SearchLimiter.VERSION);
        map.put(Fields.MILESTONE, SearchLimiter.MILESTONE);
        mapping = Collections.unmodifiableMap(map);
    };
    
      public Fields getField(SearchLimiter slimiter) {
        for (Fields f : mapping.keySet()) {
            if (mapping.get(f) == slimiter) {
                return f;
            }
        }
        return null;
    }
      
    

}
