/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orius.event.handler;

import euler.bugzilla.beans.conf.BugTableBean;
import euler.bugzilla.model.BugInfo;
import euler.bugzilla.model.conf.PositionComparator;
import euler.bugzilla.model.conf.QueryTableColumn;
import euler.bugzilla.model.conf.QueryTableConf;
import euler.bugzilla.model.query.Query;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import javafx.event.EventHandler;
import org.apache.log4j.Logger;
import orius.OriusContext;
import orius.event.ChangeColumnWidthEvent;

/**
 *
 * @author agime
 */
public class ChangeColumnWidthHandler implements EventHandler<ChangeColumnWidthEvent> {

    @Override
    public void handle(ChangeColumnWidthEvent t) {
        try {
            BugTableBean btb = new BugTableBean(OriusContext.PERSISTENCEUNIT);          
            QueryTableConf qtc = btb.findConfiguration(t.getQuery());
            List<QueryTableColumn> columns = qtc.getColumns();
            Collections.sort(columns, new PositionComparator());
            for (QueryTableColumn qtcol : columns){
                //Logger.getLogger(ChangeColumnWidthHandler.class).trace("TableColumnId: " +t.getTableColumn().getId() );
                //Logger.getLogger(ChangeColumnWidthHandler.class).trace("ConfColumnName: " +qtcol.getName());
                //Logger.getLogger(ChangeColumnWidthHandler.class).trace("Widht: "+t.getWidth());
                if(t.getTableColumn().getId().equals(BugInfo.generatePropertyName(qtcol.getName()))){
                    qtcol.setWidth(t.getWidth());
                    btb.merge(qtcol);            
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(ChangeColumnWidthHandler.class).error(ex);
        }
    }
    
}
