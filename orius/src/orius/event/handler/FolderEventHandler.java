/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orius.event.handler;

import euler.bugzilla.beans.query.QueryBean;
import euler.bugzilla.model.query.QueryFolder;
import euler.bugzilla.utils.EulerExceptionUtils;
import java.io.IOException;
import java.util.logging.Level;
import javafx.event.EventHandler;
import javafx.scene.control.TreeItem;
import jfxtras.labs.dialogs.MonologFX;
import org.apache.log4j.Logger;
import orius.OriusContext;
import orius.event.FolderEvent;

/**
 *
 * @author agime
 */
public class FolderEventHandler implements EventHandler<FolderEvent> {

    private QueryBean qs;
    private EulerExceptionUtils guiLogger = EulerExceptionUtils.getInstance();
    
    public FolderEventHandler() {
        try {
            qs = new QueryBean(OriusContext.PERSISTENCEUNIT);
        } catch (IOException ex) {
            guiLogger.handleException(this.getClass(), ex);;
        }
    }

    @Override
    public void handle(FolderEvent t) {
        if (t.getEventType() == FolderEvent.FOLDER_CREATED) {
            handleFolderCreated(t);
        }
        if (t.getEventType() == FolderEvent.DELETE_FOLDER) {
            handleFolderDeleted(t);
        }
    }

    private void handleFolderCreated(FolderEvent t) {
        Logger.getLogger(this.getClass()).debug("Caught event... " + t);
        TreeItem source = (TreeItem) t.getSource();
        QueryFolder sourceQueryFolder = (QueryFolder) source.getValue();
        qs.persist(sourceQueryFolder);
        source.setValue(sourceQueryFolder);
    }

    private void handleFolderDeleted(FolderEvent t) {
        Logger.getLogger(this.getClass()).debug("Caught event... " + t);
        TreeItem source = (TreeItem) t.getSource();
        QueryFolder sourceQueryFolder = (QueryFolder) source.getValue();
        sourceQueryFolder = qs.findFolderFetchQueries(sourceQueryFolder.getId());
        if (qs.getQueries(sourceQueryFolder).isEmpty()) {
            qs.remove(sourceQueryFolder);
            source.getParent().getChildren().remove(source);
            MonologFX dialog = new MonologFX(MonologFX.Type.INFO);                       
            dialog.setTitleText("Title text");
            dialog.setMessage("Folder has been removed from tree.");
            dialog.showDialog();
        } else {
            MonologFX dialog = new MonologFX(MonologFX.Type.ERROR);
            dialog.setTitleText("Title text");
            dialog.setMessage("Folder is not empty. Please delete all queries before proceeding");
            dialog.showDialog();
        }
    }

}
