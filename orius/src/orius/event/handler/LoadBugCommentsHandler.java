/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orius.event.handler;

import euler.bugzilla.beans.BugInfoBean;
import euler.bugzilla.event.LoadBugCommentsEvent;
import euler.bugzilla.j2bugzilla.base.Comment;
import euler.bugzilla.model.BugInfo;
import euler.bugzilla.model.ServerInformation;
import euler.bugzilla.service.CommentService;
import euler.bugzilla.utils.EulerExceptionUtils;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javafx.collections.ObservableList;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import orius.OriusContext;

/**
 *
 * @author agime
 */
public class LoadBugCommentsHandler implements EventHandler<LoadBugCommentsEvent> {

    private final ScrollPane commentsPane;
    private ServerInformation serverCredentials;

    public LoadBugCommentsHandler(ServerInformation credentials, ScrollPane commentsPane) {
        this.commentsPane = commentsPane;
        this.serverCredentials = credentials;
    }

    @Override
    public void handle(LoadBugCommentsEvent t) {
        addCommentsToScene(t);
    }

    private void addCommentsToScene(LoadBugCommentsEvent t) {
        try {
            final VBox commentsBox = new VBox();
            commentsBox.setSpacing(5);
            commentsBox.setPadding(new Insets(15, 12, 15, 12));    //top, right, bottom, left          
            final BugInfoBean bnb = new BugInfoBean(OriusContext.PERSISTENCEUNIT);
            final BugInfo bug =  bnb.find(t.getBugId());
            final CommentService commentService = new CommentService(t.getBugId(), OriusContext.getBugzillaConnector(serverCredentials));
            final ProgressIndicator pi = new ProgressIndicator();
            pi.progressProperty().bind(commentService.progressProperty());
            pi.visibleProperty().bind(commentService.runningProperty());
            pi.prefHeightProperty().bind(commentsPane.heightProperty());
            pi.prefWidthProperty().bind(commentsPane.widthProperty());
            commentsPane.setContent(pi);
            commentService.start();
            commentService.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

                @Override
                public void handle(WorkerStateEvent t) {
                    CommentService sourceService = (CommentService) t.getSource();
                    commentsBox.getChildren().add(createCommentsPaneHeader(bug)); 
                    commentsBox.getChildren().add(createAdditionalInfoHeader(bug));
                    ObservableList<Comment> comments = sourceService.getValue();
                    for (Comment c : comments) {
                        final VBox cBox = createCommentBox(c);
                        commentsBox.getChildren().add(cBox);
                    }
                    pi.prefWidthProperty().unbind();
                    pi.prefHeightProperty().unbind();
                    commentsPane.setContent(commentsBox);
                }
            });
        } catch (IOException ex) {
            EulerExceptionUtils.getInstance().handleException(this.getClass(), ex);
        }
    }

    private Node createCommentsPaneHeader(BugInfo bug) {
        StringBuilder sb = new StringBuilder("Bug ");
        TextField title = new TextField();
        sb.append(bug.getId());
        sb.append(" - ");
        sb.append(bug.getSummary());
        title.setEditable(false);
        title.setText(sb.toString());
        title.getStyleClass().add("bugHeader");
        title.prefWidthProperty().bind(commentsPane.widthProperty());
        return title;
    }
    
    private Node createAdditionalInfoHeader(BugInfo bug){
        HBox additionalHeaders = new HBox();
        additionalHeaders.setSpacing(5);
        additionalHeaders.setPadding(new Insets(3, 0, 3, 0)); //top, right, bottom, left  
        // create url field
        StringBuilder sb = new StringBuilder("[Url: ");
        TextField url = new TextField();
        sb.append(bug.getUrl());
        sb.append("]");
        url.setEditable(false);
        url.setText(sb.toString());
        additionalHeaders.getChildren().add(url);
        //create deadline field
        TextField deadline = new TextField();
        sb = new StringBuilder("[Deadline: ");
        sb.append(bug.getDeadline());
        sb.append("] ");
        deadline.setEditable(false);
        deadline.setText(sb.toString());
        additionalHeaders.getChildren().add(deadline);
        
        for(Node n :additionalHeaders.getChildren()){
            if(n instanceof TextField){
                TextField tf = (TextField) n;
                tf.getStyleClass().add("bugAdditionalInfoHeader");
                tf.prefWidthProperty().bind(commentsPane.widthProperty());
            }
        }
        return additionalHeaders;        
    }
    private VBox createCommentBox(Comment c) {
        DateFormat sdf = new SimpleDateFormat("dd/MM/YY hh:mm");
        VBox vbox = new VBox();
        
        Label header = new Label("Comment inserted by: " + c.getAuthor() + " on " + sdf.format(c.getTime()));        
        TextArea comment = new TextArea(c.getText());
        header.prefWidthProperty().bind(commentsPane.widthProperty());
        comment.prefWidthProperty().bind(commentsPane.widthProperty());
        header.setWrapText(true);
        comment.setWrapText(true); 
        comment.setEditable(false);
        comment.minHeight(25);
        
        header.getStyleClass().add("commentHeader");
        comment.getStyleClass().add("commentText");
        vbox.setSpacing(5);
        vbox.getChildren().add(header);
        vbox.getChildren().add(comment);
        vbox.getStyleClass().add("commentVBox");

        return vbox;
    }

}
