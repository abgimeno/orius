/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package orius.fx.tab;

import javafx.beans.binding.DoubleBinding;
import javafx.scene.control.TabPane;
import org.apache.log4j.Logger;

/**
 *
 * @author agime
 */
public class QueryTabPane extends TabPane{
    
            
    public QueryTabPane() {                
        setTabClosingPolicy(TabPane.TabClosingPolicy.SELECTED_TAB);
    }
    
    public QueryTab getTab(QueryTab tab){        
        return (QueryTab)this.getTabs().get(getTabs().indexOf(tab));        
    }
    
            
    public DoubleBinding minTableHeightProperty(){
        return new DoubleBinding() {
            
            {   
                super.bind(heightProperty());
            }
            @Override
            protected double computeValue() {
                double correction = 55.0;  
                double tabPaneHeight = heightProperty().doubleValue();
                double tabHeight = tabMinHeightProperty().doubleValue();
                double result = tabPaneHeight - (tabHeight + correction);
                StringBuilder sb = new StringBuilder("Computing table dimensions: ");
                sb.append(tabPaneHeight);
                sb.append(" - (");
                sb.append(tabHeight);
                sb.append(" + ");
                sb.append(correction);
                sb.append(")");
                sb.append(" = ");
                sb.append(result);                
                Logger.getLogger(this.getClass()).trace(sb.toString());
                return result;
            }
        };
    }    
}
