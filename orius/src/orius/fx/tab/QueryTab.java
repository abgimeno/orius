/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package orius.fx.tab;

import euler.bugzilla.fx.table.FilterPane;
import euler.bugzilla.dialogfx.DialogFX;
import euler.bugzilla.model.BugInfo;
import euler.bugzilla.model.query.Query;
import euler.bugzilla.utils.BugInfoExportUtilities;
import euler.bugzilla.fx.table.BugTableView;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.ToolBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import orius.event.ChangeColumnWidthEvent;
import orius.event.QueryEvent;
import orius.fx.button.SyncroniseButton;

/**
 *
 * @author agime
 */
public class QueryTab extends Tab {

    private final Query query;
    private final ToolBar bar;
    private DialogFX dialog;
    private final SyncroniseButton syncronise;
    private final Button exportToExcel = new Button("Export to Excel", new ImageView(new Image(QueryTab.class.getResourceAsStream("/orius/media/icons/excel-icon.png"))));//, new ImageView(new Image(createUrl(mediaPath, "excel-icon.png").toString()))        
    private final Button configureColumns = new Button("Configure Columns", new ImageView(new Image(QueryTab.class.getResourceAsStream("/orius/media/icons/configure_30.png"))));//, new ImageView(new Image(createUrl(mediaPath, "excel-icon.png").toString()))        
    private BugTableView results;
    private final VBox vbox = new VBox();
    private ProgressIndicator progressIndicator = new ProgressIndicator();

    public QueryTab(Query query, BugTableView resultTable) {
        syncronise = new SyncroniseButton(query);
        this.bar = new ToolBar();
        this.query = query;
        this.results = resultTable;
        initialiseCompoments();
    }

    private void initialiseCompoments() {
        StringBuilder sb = new StringBuilder(Long.toString(query.getId()));
        sb.append(query.getName());
        setId(sb.toString());
        setText(query.getDisplayName());
        setClosable(true); //true by default     
        this.results.setId(getText());
        vbox.getChildren().add(bar);
        vbox.getChildren().add(results);        
        bar.getItems().add(syncronise);
        bar.getItems().add(exportToExcel);        
        exportToExcel.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                List<TableView<BugInfo>> list = new ArrayList<>();
                list.add(results);
                dialog = BugInfoExportUtilities.exportToExcel(results.getId(), list);
                dialog.showDialog();
            }
        });
        configureColumns.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {      
                QueryEvent cte = new QueryEvent(query, QueryEvent.OPEN_COL_CONF);
                Event.fireEvent(t.getTarget(), cte);
            }
        });
        bar.getItems().add(configureColumns);
        setContent(vbox);
        progressIndicator.setMaxHeight(20);
        progressIndicator.setMaxWidth(20);                
        setColumWidthListener();
        bar.getItems().add(results.getFilter());
    }

    public Query getQuery() {
        return query;
    }

    public BugTableView getResults() {
        return results;
    }

    public void setResults(BugTableView results) {
        this.results = results;
    }

    public ProgressIndicator getProgressIndicator() {
        return progressIndicator;
    }

    public void setProgressIndicator(ProgressIndicator progressIndicator) {
        this.progressIndicator = progressIndicator;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + Objects.hashCode(this.query);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final QueryTab other = (QueryTab) obj;
        if (!Objects.equals(this.query, other.query)) {
            return false;
        }
        return true;
    }

    private void setColumWidthListener() {
        for (Object obj: results.getColumns()){
            final TableColumn col = (TableColumn) obj;
            col.widthProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object oldValue, Object newValue) {
                Double newWidth = (Double) newValue;
                ChangeColumnWidthEvent ccwe = new ChangeColumnWidthEvent(newWidth, query, col, ChangeColumnWidthEvent.CHANGED);                
                Event.fireEvent(results, ccwe);                
            }
        });
        }    }

}
