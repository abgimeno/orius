/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package orius.fx.button;

import euler.bugzilla.model.query.Query;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import orius.event.QueryEvent;

/**
 *
 * @author agime
 */
public class ConfigureQueryButton extends Button{

    public ConfigureQueryButton(String string, final Query query) {
        super(string);
        setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {      
                QueryEvent cte = new QueryEvent(query, QueryEvent.CONFIGURE_COLUMNS);
                Event.fireEvent(t.getTarget(), cte);
            }
        });
    }
    
}
