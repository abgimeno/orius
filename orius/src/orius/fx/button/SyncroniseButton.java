/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package orius.fx.button;

import euler.bugzilla.model.query.Query;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import org.apache.log4j.Logger;
import orius.event.QueryEvent;
import orius.fx.tab.QueryTab;

/**
 *
 * @author agime
 */
public class SyncroniseButton extends Button {
    
    public SyncroniseButton(final Query query) {
        super("Syncronise Query", new ImageView(new Image(QueryTab.class.getResourceAsStream("/orius/media/icons/refresh_30.png"))));   
        setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                    QueryEvent que = new QueryEvent(query,QueryEvent.SYNCRONISE);                    
                    Logger.getLogger(this.getClass().getName()).debug(this.getClass().getEnclosingMethod()+"--Firing:  "+ que);
                    Event.fireEvent(t.getTarget(),que); //one event to create the table
            }
        });
    }
    
}
