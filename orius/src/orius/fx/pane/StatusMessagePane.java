/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package orius.fx.pane;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;

/**
 *
 * @author agime
 */
public class StatusMessagePane extends StackPane {
    
    private HBox hbox = new HBox();
    private Label statusMessage = new Label();
    private Button closeButton = new Button("Close");

    public StatusMessagePane() {
        super();
        setVisible(false);
        setAlignment(Pos.CENTER_LEFT);
        setAlignment(statusMessage, Pos.CENTER_LEFT);
        setAlignment(closeButton, Pos.CENTER_RIGHT);
        hbox.setPadding(new Insets(5,5,5,5));
        hbox.getChildren().add(statusMessage);
        hbox.getChildren().add(closeButton);
        getChildren().add(hbox);
    }
    
    public void setMessageText(String text){
        statusMessage.setText(text);
    }
    public String getMessageText(){
        return statusMessage.getText();
    }

    public Label getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(Label statusMessage) {
        this.statusMessage = statusMessage;
    }

    public Button getCloseButton() {
        return closeButton;
    }

    public void setCloseButton(Button closeMessage) {
        this.closeButton = closeMessage;
    }
    public void displayPane(){
        setVisible(true);
    }
}
