package orius.fx.tree;

import euler.bugzilla.beans.query.QueryBean;
import euler.fx.tree.TreeElement;
import euler.bugzilla.model.query.TreeTypeEnum;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import org.apache.log4j.Logger;

/**
 *
 * @author agime
 */
public class BugTreeCell extends TreeCell<TreeElement> {

    private TextField name;

    private final Node server = new ImageView(new Image(getClass().getResourceAsStream("/orius/media/icons/server.png")));
    private final Node query = new ImageView(new Image(getClass().getResourceAsStream("/orius/media/icons/buggie.png")));
    private final Node folder = new ImageView(new Image(getClass().getResourceAsStream("/orius/media/icons/folder-icon.png")));

    private ContextMenu contextMenu = new ContextMenu();
    private QueryBean qs;

    public BugTreeCell() {
        try {
            this.qs = new QueryBean("oriusPU");
        } catch (IOException ex) {
            Logger.getLogger(this.getClass()).error("", ex);
        }
    }

    @Override
    public void startEdit() {
        super.startEdit();

        if (name == null) {
            createTextField("");
        }
        setText(null);
        setGraphic(name);
        name.selectAll();
    }

    @Override
    public void cancelEdit() {
        super.cancelEdit();
        setText(getDisplayName());
        setGraphic(chooseGraphic());
    }

    @Override
    public void updateItem(TreeElement item, boolean empty) {
        super.updateItem(item, empty);

        if (empty) {
            setText(null);
            setGraphic(null);
        } else {

            /* 
             * the code below where we check if  item is leaf or parent
             * is null, etc. etc. has too many ifs
             */
            if (getTreeItem().getParent() == null) {
                createServerCell();
            } else {
                if (getItem().getType() == TreeTypeEnum.FOLDER) {
                    createFolderCell();
                }
                if (getItem().getType() == TreeTypeEnum.QUERY) {
                    createQueryCell();
                }
                if (getItem().getType() == TreeTypeEnum.SERVER) {
                    createServerCell();
                }
            }

            if (isEditing()) {
                if (name != null) {
                    name.setText(getName());
                }
                setText(null);
                setGraphic(name);
            } else {
                setText(getDisplayName());
                setGraphic(getTreeItem().getGraphic());
                setContextMenu(contextMenu);
            }
        }

    }

    private void createServerCell() {
        name = createLabel(server);
        contextMenu = new ContextMenu();
        contextMenu.getItems().addAll(TreeBuilder.getServerMenuItems(getTreeItem()));
    }

    private void createFolderCell() {
        name = createLabel(folder);
        contextMenu = new ContextMenu();
        contextMenu.getItems().addAll(TreeBuilder.getFolderMenuItems(getTreeItem()));

    }

    private void createQueryCell() {
        name = createLabel(query);
        contextMenu = new ContextMenu();
        contextMenu.getItems().addAll(TreeBuilder.getQueryMenuItems(getTreeItem()));

    }

    private String getName() {
        return getItem() == null ? "" : getItem().getName();
    }

    private String getDisplayName() {
        return getItem() == null ? "" : getItem().getDisplayName();
    }

    private TextField createLabel(Node graphic) {
        TextField t = createTextField(getName());
        getTreeItem().setGraphic(graphic);
        return t;
    }

    private TextField createTextField(String text) {
        TextField tf = new TextField(text);
        tf.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent t) {
                if (t.getCode() == KeyCode.ENTER) {
                    TextField tf = (TextField) t.getSource();
                    getItem().setName(tf.getText());
                    commitEdit(getItem());
                    if (getItem().getId() == null) { //it's a new Folder
                        qs.persist(getItem());
                    } else { //it's an update of the name
                        TreeElement te = qs.find(getItem().getId(), getItem().getType());
                        updateQueryName(tf, te);
                    }
                } else if (t.getCode() == KeyCode.ESCAPE) {
                    cancelEdit();
                }
            }

            private void updateQueryName(TextField tf, TreeElement te) {
                if (getItem().getType() == TreeTypeEnum.QUERY) {
                    String regex = "(.*)(\\([0-9]\\))"; //regular expression to append the number at the end of the query name
                    String displayName = getItem().getDisplayName();
                    Pattern p = Pattern.compile(regex);
                    Matcher m = p.matcher(displayName);
                    if (m.matches()) {
                        te.setName(tf.getText());
                        qs.merge(te);
                        setText(tf.getText() + m.group(2));
                    }
                } else {
                    te.setName(tf.getText());
                    qs.merge(te);
                    setText(tf.getText());
                }
            }
        });
        return tf;
    }

    private Node chooseGraphic() {
        if (getItem().getType() == TreeTypeEnum.FOLDER) {
            return folder;
        }
        if (getItem().getType() == TreeTypeEnum.QUERY) {
            return query;
        }
        if (getItem().getType() == TreeTypeEnum.SERVER) {
            return server;
        }
        return null;
    }
}
