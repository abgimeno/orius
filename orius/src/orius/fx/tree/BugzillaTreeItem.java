/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package orius.fx.tree;

import euler.fx.tree.TreeElement;
import java.util.Objects;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;

/**
 *
 * @author agime
 */
public class BugzillaTreeItem extends TreeItem<TreeElement> {

    public BugzillaTreeItem() {

    }

    public BugzillaTreeItem(TreeElement t) {
        super(t);
    }

    public BugzillaTreeItem(TreeElement t, Node node) {
        super(t, node);
    }

    @Override
    public int hashCode() {
        int hash = 25;
        hash = 19 * hash + Objects.hashCode(this.getValue().getId());
        hash = 14 * hash + Objects.hashCode(this.getValue().getName());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if ((getClass() != obj.getClass()) && (TreeItem.class != obj.getClass())) {
            return false;
        }
        if (obj instanceof TreeItem) {
            final TreeItem other = (TreeItem) obj;
            return Objects.equals(this.getValue(), other.getValue());
        }
        if (obj instanceof BugzillaTreeItem) {
            final BugzillaTreeItem other = (BugzillaTreeItem) obj;
            return Objects.equals(this.getValue(), other.getValue());
        }
        return false;
    }

}
