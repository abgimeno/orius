/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package orius.fx.tree;

import euler.bugzilla.beans.BugzillaCredentialsBean;
import euler.bugzilla.j2bugzilla.enums.Action;
import euler.bugzilla.model.BugInfo;
import euler.bugzilla.model.ProductInfo;
import euler.bugzilla.model.ServerInformation;
import euler.bugzilla.model.query.Query;
import euler.bugzilla.model.query.QueryFolder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javax.persistence.NoResultException;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import orius.OriusContext;
import orius.event.FolderEvent;
import orius.event.QueryEvent;
import orius.event.handler.FolderEventHandler;
import orius.query.QueryCreatorController;

/**
 *
 * @author agime
 */
public class TreeBuilder {

    public static List<MenuItem> getServerMenuItems(final TreeItem item) {
        List<MenuItem> items = new ArrayList<>();
        MenuItem serverConf = new MenuItem("Configure Server", new ImageView(new Image(TreeBuilder.class.getResourceAsStream("/orius/media/icons/configure.png"))));
        MenuItem productsConf = new MenuItem("Update products", new ImageView(new Image(TreeBuilder.class.getResourceAsStream("/orius/media/icons/editproducts.png"))));
        MenuItem addFolder = new MenuItem("Add folder", new ImageView(new Image(TreeBuilder.class.getResourceAsStream("/orius/media/icons/add.png"))));
        serverConf.setOnAction(new EventHandler() {
            @Override
            public void handle(Event t) {
                Map<Class, Object> params = new HashMap<>();
                params.put(TreeItem.class, item);
                try {
                    BugzillaCredentialsBean bcs = new BugzillaCredentialsBean(OriusContext.PERSISTENCEUNIT);
                    ServerInformation bc = bcs.getCredentials();
                    params.put(Action.class, Action.MODIFY);
                    params.put(ServerInformation.class, bc);
                    OriusContext.loadScreen(OriusContext.LOGINSCREEN, new Stage(), params);
                } catch (NoResultException ex) {
                    params.put(Action.class, Action.CREATE);
                    OriusContext.loadScreen(OriusContext.LOGINSCREEN, (Stage) item.getGraphic().getScene().getWindow(), params);
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass()).error(Level.ERROR, ex);
                }
            }
        });
        addFolder.setOnAction(new EventHandler() {
            @Override
            public void handle(Event t) {
                try {
                    QueryFolder folder = new QueryFolder();
                    folder.setName("Folder");
                    TreeItem<QueryFolder> subfolder = new TreeItem(folder);
                    item.getChildren().add(subfolder);
                    subfolder.addEventHandler(FolderEvent.FOLDER_CREATED, new FolderEventHandler());
                    subfolder.addEventHandler(FolderEvent.DELETE_FOLDER, new FolderEventHandler());
                    FolderEvent que = new FolderEvent(FolderEvent.FOLDER_CREATED);
                    Logger.getLogger(this.getClass().getName()).debug(this.getClass().getEnclosingMethod() + "--Firing:  " + que);
                    Event.fireEvent(subfolder, que);
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass()).error(null, ex);
                }

            }
        });
        productsConf.setOnAction(new EventHandler() {
            @Override
            public void handle(Event t) {
                try {
                    Map<Class, Object> params = new HashMap<>();
                    params.put(Action.class, Action.MODIFY);
                    OriusContext.loadScreen(OriusContext.PRODUCTS, new Stage(), params);
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass()).error(null, ex);
                }

            }
        });
        items.add(serverConf);
        items.add(addFolder);
        items.add(productsConf);
        return items;

    }

    public static List<MenuItem> getFolderMenuItems(final TreeItem item) {
        List<MenuItem> items = new ArrayList<>();
        MenuItem addQuery = new MenuItem("Add query", new ImageView(new Image(TreeBuilder.class.getResourceAsStream("/orius/media/icons/add.png"))));
        addQuery.setOnAction(new EventHandler() {
            @Override
            public void handle(Event t) {
                try {
                    Logger.getLogger(this.getClass().getName()).debug(this.getClass().getEnclosingMethod() + " calling controller");
                    final Stage stage = new Stage();
                    QueryCreatorController lc = (QueryCreatorController) OriusContext.loadScreen(OriusContext.QUERYCREATOR, stage);
                    lc.setParentFolder((QueryFolder) item.getValue());
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass()).error(Level.ERROR, ex);
                }
            }
        });
        MenuItem delete = new MenuItem("Delete folder", new ImageView(new Image(TreeBuilder.class.getResourceAsStream("/orius/media/icons/delete.png"))));
        delete.setOnAction(new EventHandler() {
            @Override
            public void handle(Event t) {
                try {
                    FolderEvent fe = new FolderEvent(FolderEvent.DELETE_FOLDER);
                    Logger.getLogger(this.getClass().getName()).debug("Firing:  " + fe);
                    Event.fireEvent(item, fe);
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass()).error(Level.ERROR, ex);
                }
            }
        });
        items.add(delete);
        items.add(addQuery);
        return items;
    }

    public static List<MenuItem> getQueryMenuItems(final TreeItem item) {
        List<MenuItem> items = new ArrayList<>();
        final MenuItem execute = new MenuItem("Execute query", new ImageView(new Image(TreeBuilder.class.getResourceAsStream("/orius/media/icons/execute.png"))));
        execute.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                try {
                    QueryEvent que = new QueryEvent((Query) item.getValue(), QueryEvent.EXECUTE);
                    Logger.getLogger(this.getClass().getName()).debug(this.getClass().getEnclosingMethod()
                            + "--Firing:  " + que.getEventType().getName());
                    Event.fireEvent(item, que);
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass()).error(Level.ERROR, ex);
                }
            }
        });
        MenuItem syncronise = new MenuItem("Syncronise results", new ImageView(new Image(TreeBuilder.class.getResourceAsStream("/orius/media/icons/refresh.png"))));
        syncronise.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                try {
                    QueryEvent que = new QueryEvent((Query) item.getValue(), QueryEvent.SYNCRONISE);
                    Logger.getLogger(this.getClass().getName()).debug(this.getClass().getEnclosingMethod()
                            + "--Firing:  " + que.getEventType().getName());
                    Event.fireEvent(item, que); //one event to create the table

                } catch (Exception ex) {
                    Logger.getLogger(this.getClass()).error(this.getClass().getEnclosingMethod(), ex);
                }
            }
        });
        MenuItem delete = new MenuItem("Delete query", new ImageView(new Image(TreeBuilder.class.getResourceAsStream("/orius/media/icons/delete.png"))));
        delete.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                try {
                    QueryEvent que = new QueryEvent((Query) item.getValue(), QueryEvent.DELETE);
                    Logger.getLogger(this.getClass().getName()).debug(this.getClass().getEnclosingMethod()
                            + "--Firing:  " + que.getEventType().getName());
                    Event.fireEvent(item, que); //one event to create the table                   
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass()).error(null, ex);
                }
            }
        });
        items.add(execute);
        items.add(syncronise);
        items.add(delete);
        return items;
    }

    public static HBox createButtonHBox(final Stage primaryStage) {
        HBox hbox = new HBox();
        hbox.setSpacing(5);
        // CREATE MIN AND CLOSE BUTTONS
        //create button for closing application
        Button close = new Button("Close me");
        close.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                //in case we would like to close whole demo
                //javafx.application.Platform.exit();

                //however we want to close only this instance of stage
                ((Button) event.getSource()).getScene().getWindow().hide();
            }
        });

        //create button for minimalising application
        Button min = new Button("Minimize me");
        min.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                primaryStage.setIconified(true);
            }
        });
        hbox.getChildren().add(close);
        hbox.getChildren().add(min);
        return hbox;
    }
}
