    /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package orius;

import euler.bugzilla.exception.QueryTableConfMissingException;
import orius.event.handler.LoadBugCommentsHandler;
import orius.event.ChangeColumnWidthEvent;
import orius.event.handler.ChangeColumnWidthHandler;
import com.sun.javafx.event.EventDispatchChainImpl;
import euler.bugzilla.beans.BugInfoBean;
import euler.bugzilla.beans.conf.BugTableBean;
import euler.bugzilla.beans.query.QueryBean;
import euler.bugzilla.builder.TableDirector;
import euler.bugzilla.event.EditBugEvent;
import euler.bugzilla.event.LoadBugCommentsEvent;
import euler.bugzilla.fx.table.BugTableView;
import euler.bugzilla.model.query.Query;
import euler.bugzilla.model.query.QueryFolder;
import euler.fx.tree.TreeElement;
import euler.bugzilla.j2bugzilla.enums.Action;
import euler.bugzilla.model.BugInfo;
import euler.bugzilla.model.query.TreeTypeEnum;
import euler.bugzilla.service.J2BugSearchService;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.*;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ToolBar;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import static orius.BaseController.PERSISTENCEUNIT;
import orius.event.FolderEvent;
import orius.event.QueryEvent;
import orius.event.handler.FolderEventHandler;
import orius.fx.tree.BugTreeCell;
import orius.fx.tree.BugzillaTreeItem;
import orius.fx.tab.QueryTabPane;
import orius.fx.tab.QueryTab;
import orius.fx.tree.TreeBuilder;

/**
 *
 * @author agime
 *
 */
public class MainWindowController extends BaseController {

    @FXML
    protected QueryTabPane tabPane;

    @FXML
    protected VBox treeVbox;

    @FXML
    protected ScrollPane commentsPane;

    @FXML//  fx:id="mainToolBar"
    protected ToolBar mainToolBar;

    protected TreeView treeView = new TreeView();

    private QueryBean qs;
    private BugInfoBean bis;
    private BugzillaTreeItem rootTree;
    private BugTableBean btb;
    
    
    private final EventDispatchChainImpl chain = new EventDispatchChainImpl();

    
    public MainWindowController() {
        try {
            qs = new QueryBean(PERSISTENCEUNIT);
            bis = new BugInfoBean(PERSISTENCEUNIT);
            btb = new BugTableBean(PERSISTENCEUNIT);
        } catch (IOException ex) {
            Logger.getLogger(this.getClass()).error("", ex);
        }
    }

    @Override // This method is called by the FXMLLoader when initialization is complete
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);
        assert root != null : "fx:id=\"root\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert tabPane != null : "fx:id=\"tabPane\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert treeVbox != null : "fx:id=\"treeVbox\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert mainToolBar != null : "fx:id=\"mainToolBar\" was not injected: check your FXML file 'MainWindow.fxml'.";
        doInitialiseTreeView();
        doInitialiseToolBar();
        doInitialiseTabPane();

    }

    private void doInitialiseTreeView() {
        rootTree = new BugzillaTreeItem(serverCredentials);
        rootTree.buildEventDispatchChain(chain);
        treeView.setCellFactory(new Callback<TreeView<TreeElement>, BugTreeCell>() {
            @Override
            public BugTreeCell call(TreeView<TreeElement> p) {
                return new BugTreeCell();// to bind a property inside to the service results
            }
        });
        for (QueryFolder qf : qs.getAllQueryFolders()) {
            BugzillaTreeItem ti = new BugzillaTreeItem(qf);
            setQueryFolderHandlers(ti);
            ti.buildEventDispatchChain(chain);
            for (Query q : qs.getQueries(qf)) {
                final BugzillaTreeItem queryItem = new BugzillaTreeItem(q);
                /*
                I created a customised chain event, but not all events are channeled through it yet
                
                The Window, Scene, and Node classes implement the EventTarget interface. 
                This means that all nodes, including windows and scenes, can respond to events. 
                The classes for some UI elements, for example, Tab, TreeItem, and MenuItem, do not inherit from the Node class. 
                They can still respond to events because they implement the EventTarget interface. 
                If you develop a custom UI element, 
                you will need to implement this interface if you want your UI element to respond to events.
                */
                queryItem.buildEventDispatchChain(chain);
                ti.getChildren().add(queryItem);
                //All query tree items will catch the Event, only 1 will respond
                queryItem.addEventHandler(QueryEvent.SYNCRONISE_DONE, new EventHandler<QueryEvent>() {
                    @Override
                    public void handle(QueryEvent t) {
                        BugzillaTreeItem sourceItem = (BugzillaTreeItem) t.getSource();
                        //Logger.getLogger(this.getClass()).trace("Caught event... " + this.getClass().getEnclosingMethod() + this.getClass().getEnclosingMethod() + t.getEventType().getName());
                        // is it me my lord
                        if (sourceItem.getValue().equals(t.getQuery())) {
                            Query sourceQuery = t.getQuery();
                            queryItem.setValue(qs.find(sourceQuery.getId()));
                        }
                    }
                });
            }
            rootTree.getChildren().add(ti);
        }
        /*
        the next lines call the methods that set the handlers that will respond on events
        I need to set handlers in the TreeView and in the root AnchorPane as they don't
        share the same events chain.       
        */
        setHandlers(rootTree);
        setHandlers(root);
        treeView.setRoot(rootTree);
        treeView.setEditable(true);
        binder.bind(treeView.prefHeightProperty(), treeVbox.heightProperty());
        treeVbox.getChildren().add(treeView);

    }

    /*
     * if there is associated data already stored on db, it retrieves from db
     * otherwise calls syncroniseQueryAndShowTab, to get the data by calling the WS
     */
    public void executeQuery(Query query) {
        ObservableList<BugInfo> items = new SimpleListProperty(FXCollections.<BugInfo>observableArrayList());
        List<BugInfo> bugs = bis.findQueryBugs(query);

        if (bugs.isEmpty()) {//here we check of there is data associated to this query on DB
            syncroniseQueryAndShowTab(query);
        } else {
            try {
                
                final BugTableView table = director.createTable(btb.findConfiguration(query));
                QueryTab tab = new QueryTab(query, table);
                if (!tabPane.getTabs().contains(tab)) {
                    Logger.getLogger(this.getClass()).info("The tab was not added, adding...");
                    tabPane.getTabs().add(tab);
                }
                items.addAll(bugs);
                table.setItems(items);
            } catch (QueryTableConfMissingException ex) {
                Logger.getLogger(MainWindowController.class).error(ex);
            }
        }

    }

    /*
     * creates awebservice call to retrieve the list of bugs and 
     * adds a new tab in the TabPane, which contains a table of the results
     */
    public void syncroniseQueryAndShowTab(final Query query) {
        
        final QueryTab tab = getTabFromPane(query);        
        final BugTableView results = tabPane.getTab(tab).getResults();

        J2BugSearchService service = new J2BugSearchService(OriusContext.getBugzillaConnector(serverCredentials), query.getParamMap());
        service.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
            @Override
            public void handle(WorkerStateEvent t) {
                J2BugSearchService sourceService = (J2BugSearchService) t.getSource();
                QueryEvent que = new QueryEvent(query, QueryEvent.SYNCRONISE_DONE);
                query.setBugs(qs.persistBugs(sourceService.getValue()));
                query.setNbugs(sourceService.getValue().size());
                qs.merge(query);
                //the below method updates number of results on tree
                chain.dispatchEvent(que);
                //Event.fireEvent(t.getTarget(), que); // I want this to be caught by TreeItemQueryItem
                //the below method update number of results on tab title
                //need to extend tabpane for custom behaviour
                tabPane.getTab(tab).setText(query.getDisplayName());
                tabPane.getTab(tab).setGraphic(null);
                //results.itemsProperty().unbind();
            }
        });

        Node tabGraphic = tab.getProgressIndicator();
        tab.setGraphic(tabGraphic);

        binder.bind(tabGraphic.visibleProperty(), service.runningProperty());
        binder.bind(tab.getProgressIndicator().progressProperty(), service.progressProperty());
        binder.bind(results.itemsProperty(), service.valueProperty());
        
        service.start();
    

    }
    /*  Checks if tab aux is already added to tab pane
     *   if tab is added then the tab returned is the existing one
     *   if tab is not added then newly created aux is added to the tab
     */

    private QueryTab getTabFromPane(Query query) {
        try {
            QueryTab aux = new QueryTab(query, director.createTable(btb.findConfiguration(query)));
            final QueryTab tab;
            if (tabPane.getTabs().contains(aux)) {
                tab = (QueryTab) tabPane.getTabs().get(tabPane.getTabs().indexOf(aux));
            } else {
                tabPane.getTabs().add(aux);
                tab = aux;
            }
            return tab;
        } catch (QueryTableConfMissingException ex) {
            Logger.getLogger(MainWindowController.class).error(ex);
            return null;
        }
    }


    public void addQueryToTree(Query q, QueryFolder parent) {
        for (TreeItem<TreeElement> ti : rootTree.getChildren()) {
            if (ti.getValue().getId() == parent.getId()) {
                BugzillaTreeItem query = new BugzillaTreeItem(q);
                ti.getChildren().add(query);
            }
        }
    }
    /*
     it removes a query from the tree, it assumes simple tree structure 
     Server->Folder->Query
     */

    public void removeQueryFromTree(Query q, QueryFolder parent) {
        for (TreeItem<TreeElement> ti : rootTree.getChildren()) {
            if (ti.getValue().getId() == parent.getId()) {
                BugzillaTreeItem query = new BugzillaTreeItem(q);
                ti.getChildren().remove(ti.getChildren().indexOf(query));
            }
        }
    }

    @Override
    protected void setController() {
        OriusContext.addController(AppControllers.MAINWINDOW, this);
    }

    /*
     TreeItem (EventTarget) and Node are 2 different animals, completely independent
     if an event is fired from a TreeItem, it cannot be caught adding a handler within a node
     so I need to deal with separated handlers for TreeItems and Node types  
    
     I've created a customised dispatch chain, as I found no other way to update the number 
     of results on the Tree, however id does not apply to other events, theoretically I could make all objects to utilise
    the same dispatch chain, but still not implemented
     */
    private void setHandlers(BugzillaTreeItem target) {
        target.addEventHandler(QueryEvent.EXECUTE, new EventHandler<QueryEvent>() {
            @Override
            public void handle(QueryEvent t) {
                Logger.getLogger(this.getClass()).debug("Caught event... " + this.getClass().getEnclosingMethod() + t.getEventType().getName());
                Query sourceQuery = t.getQuery();
                executeQuery(sourceQuery);

            }
        });
        target.addEventHandler(QueryEvent.SYNCRONISE, new EventHandler<QueryEvent>() {
            @Override
            public void handle(QueryEvent t) {
                Logger.getLogger(this.getClass()).debug("Caught event... " + this.getClass().getEnclosingMethod() + this.getClass().getEnclosingMethod() + t.getEventType().getName());
                Query sourceQuery = t.getQuery();
                syncroniseQueryAndShowTab(sourceQuery);
            }
        });
        target.addEventHandler(QueryEvent.DELETE, new EventHandler<QueryEvent>() {
            @Override
            public void handle(QueryEvent t) {
                Logger.getLogger(this.getClass()).debug("Caught event... " + this.getClass().getEnclosingMethod() + this.getClass().getEnclosingMethod() + t.getEventType().getName());
                Query sourceQuery = t.getQuery();
                qs.remove(sourceQuery);                                
                removeQueryFromTree(sourceQuery, sourceQuery.getFolder());//source.getParent().getChildren().remove(source);
            }
        });
    }
    /*
    set Handlers for Node type objects
    */
    private void setHandlers(Node node) {
        node.addEventHandler(ChangeColumnWidthEvent.CHANGED, new ChangeColumnWidthHandler());
        node.addEventHandler(LoadBugCommentsEvent.LOAD_BUGS, new LoadBugCommentsHandler(serverCredentials, commentsPane));        
        node.addEventHandler(EditBugEvent.EDIT, new EventHandler<EditBugEvent>() {

            @Override
            public void handle(EditBugEvent t) {
                try {
                    Logger.getLogger(this.getClass()).debug("Caught event... " + this.getClass().getEnclosingMethod() + t.getEventType().getName());
                    Map<Class, Object> params = new HashMap<>();
                    BugInfo selectedBug = bis.find(t.getBugId());
                    params.put(BugInfo.class, selectedBug);
                    params.put(Action.class, Action.MODIFY);
                    OriusContext.loadScreen(OriusContext.UPDATE_BUG, new Stage(), params);
                } catch (Exception ex) {
                    guiLog.handleException(this.getClass(), ex);
                }
            }
        });
        node.addEventHandler(QueryEvent.EXECUTE, new EventHandler<QueryEvent>() {
            @Override
            public void handle(QueryEvent t) {
                Logger.getLogger(this.getClass()).debug("Caught event... " + this.getClass().getEnclosingMethod() + t.getEventType().getName());
                Query sourceQuery = t.getQuery();
                executeQuery(sourceQuery);

            }
        });
        node.addEventHandler(QueryEvent.SYNCRONISE, new EventHandler<QueryEvent>() {
            @Override
            public void handle(QueryEvent t) {
                Logger.getLogger(this.getClass()).debug("Caught event... " + this.getClass().getEnclosingMethod() + this.getClass().getEnclosingMethod() + t.getEventType().getName());
                Query sourceQuery = t.getQuery();
                syncroniseQueryAndShowTab(sourceQuery);
            }
        });
        node.addEventHandler(QueryEvent.DELETE, new EventHandler<QueryEvent>() {
            @Override
            public void handle(QueryEvent t) {
                Logger.getLogger(this.getClass()).debug("Caught event... " + this.getClass().getEnclosingMethod() + this.getClass().getEnclosingMethod() + t.getEventType().getName());
                Query sourceQuery = t.getQuery();
                qs.remove(sourceQuery);
                removeQueryFromTree(sourceQuery, sourceQuery.getFolder());//source.getParent().getChildren().remove(source);
            }
        });
        node.addEventHandler(QueryEvent.OPEN_COL_CONF, new EventHandler<QueryEvent>() {

            @Override
            public void handle(QueryEvent t) {
                try {
                    Map<Class, Object> params = new HashMap<>();
                    params.put(Query.class, t.getQuery());
                    OriusContext.loadScreen(OriusContext.COLUMCONF_SCREEN, new Stage(), params);
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass()).error(Level.ERROR, ex);
                }
            }
        });
    }
    /*
    set handlers to deal with particular Folder type events
    */
    private void setQueryFolderHandlers(TreeItem<TreeElement> queryFolder) {
        root.addEventHandler(FolderEvent.FOLDER_CREATED, new FolderEventHandler());
        root.addEventHandler(FolderEvent.DELETE_FOLDER, new FolderEventHandler());
    }

    @Override
    public void customInitialiser() {
        Logger.getLogger(this.getClass()).info("Custom initialiser not implemented for this controller."); //To change body of generated methods, choose Tools | Templates.
    }

    private void doInitialiseTabPane() {
        tabPane.heightProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object oldValue, Object newValue) {
                //Logger.getLogger(this.getClass()).trace("Old height: " + oldValue);
                //Logger.getLogger(this.getClass()).trace("New height: " + newValue);               
            }
        });
    }

    private void doInitialiseToolBar() {
        Logger.getLogger(this.getClass()).info("initialising tool bar");
        Button addBug = new Button("Add Bug", new ImageView(new Image(TreeBuilder.class.getResourceAsStream("/orius/media/icons/addbug.png"))));//, new ImageView(new Image(createUrl(mediaPath, "excel-icon.png").toString()))        
        addBug.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                try {
                    OriusContext.loadScreen(OriusContext.SELECT_PRODUCT, new Stage());
                } catch (Exception ex) {
                    guiLog.handleException(this.getClass(), ex);
                }
            }
        });
        //Button addServer = new Button("Add Server", new ImageView(new Image(TreeBuilder.class.getResourceAsStream("/orius/media/icons/excel-icon.png"))));//, new ImageView(new Image(createUrl(mediaPath, "excel-icon.png").toString()))        
        Button getBugs = new Button("Get new Bugs", new ImageView(new Image(TreeBuilder.class.getResourceAsStream("/orius/media/icons/get_30.png"))));//, new ImageView(new Image(createUrl(mediaPath, "excel-icon.png").toString()))                
        getBugs.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {

                analyseChildren(rootTree);

            }
            
            private void analyseChildren(TreeItem<TreeElement> parent) {
                for (TreeItem<TreeElement> ti : parent.getChildren()) {
                    if (!parent.isLeaf()) {
                        analyseChildren(ti);                        
                    }                    
                    if (ti.getValue().getType() == TreeTypeEnum.QUERY) {
                        Logger.getLogger(this.getClass()).debug("Syncronising " + ti.getValue().getDisplayName());
                        syncroniseQueryTreeItem(ti);
                    }
                }
            }

        });
        //mainToolBar.getItems().add(getBugs);
        mainToolBar.getItems().add(addBug);
        //mainToolBar.getItems().add(addServer);

    }
    public void syncroniseQueryTreeItem(final TreeItem<TreeElement> treeItem) {
        if (!(treeItem.getValue().getType() == TreeTypeEnum.QUERY)) {
            return;
        }
        if (treeItem.getValue().getType() == TreeTypeEnum.QUERY) {            
            J2BugSearchService service = getUpdateService(treeItem);
            service.start();
        }
    }
    public J2BugSearchService getUpdateService(final TreeItem<TreeElement> treeItem) {
            final Query query = (Query) treeItem.getValue();
            J2BugSearchService service = new J2BugSearchService(OriusContext.getBugzillaConnector(serverCredentials), query.getParamMap());           
            service.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
                @Override
                public void handle(WorkerStateEvent t) {
                    J2BugSearchService sourceService = (J2BugSearchService) t.getSource();
                    QueryEvent que = new QueryEvent(query, QueryEvent.SYNCRONISE_DONE);
                    query.setBugs(qs.persistBugs(sourceService.getValue()));
                    query.setNbugs(sourceService.getValue().size());
                    qs.merge(query);
                    //the below call updates number of results on tree
                    chain.dispatchEvent(que);
                }
            });

            ProgressIndicator pi = new ProgressIndicator();
            treeItem.setGraphic(pi);

            binder.bind(pi.visibleProperty(), service.runningProperty());
            binder.bind(pi.progressProperty(), service.progressProperty());

            return service;
        }

}
