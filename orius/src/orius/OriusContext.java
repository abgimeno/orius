/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package orius;

import euler.bugzilla.j2bugzilla.BugzillaConnector;
import euler.bugzilla.j2bugzilla.base.BugzillaException;
import euler.bugzilla.j2bugzilla.base.ConnectionException;
import euler.bugzilla.j2bugzilla.rpc.BugzillaVersion;
import euler.bugzilla.j2bugzilla.rpc.LogIn;
import euler.bugzilla.model.ServerInformation;
import euler.bugzilla.utils.EulerExceptionUtils;
import euler.fx.controller.AppController;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import javafx.fxml.FXMLLoader;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.apache.log4j.Logger;

/**
 *
 * @author root
 */
public class OriusContext {

    public static final String LOGINSCREEN = "/orius/conf/login/LoginCredentials.fxml";
    public static final String MAINSCREEN = "/orius/MainWindow.fxml";
    public static final String PRODUCTS = "/orius/conf/product/ProductsConfiguration.fxml";
    public static final String QUERYCREATOR = "/orius/query/QueryCreator.fxml";
    public static final String UPDATE_BUG = "/orius/bug/BugUpdate.fxml";
    public static final String SELECT_PRODUCT = "/orius/bug/SelectProduct.fxml";
    public static final String COLUMCONF_SCREEN = "/orius/conf/table/ColumnConfiguration.fxml";

    private static OriusContext instance;
    private static Orius app;
    private static final Map<AppControllers, AppController<Orius>> controllers = new HashMap<>();    
    private static final Map<Long, ServerInformation> servers = new HashMap<>();
    
    private static Image icon = new Image("/orius/media/icons/thunderbug-icon.png");

    private static LogIn login;
    protected static ServerInformation bc;
    public static final String PERSISTENCEUNIT = "oriusPU";
    private static String DEFAULT_CSS = "/modena_full.css";
    
    public static OriusContext getInstance() {
        if (instance == null) {
            instance = new OriusContext();
        }
        return instance;
    }

    public static void setApplication(Orius app) {
        OriusContext.app = app;
    }

    public static Orius getApplication() {
        return app;
    }

    public static void addController(AppControllers controller, AppController<Orius> clasz) {
        OriusContext.controllers.put(controller, clasz);

    }

    public static AppController getController(AppControllers controller) {
        return OriusContext.controllers.get(controller);
    }

    public static BugzillaConnector getBugzillaConnector(ServerInformation si) {
        BugzillaConnector conn = new BugzillaConnector();
        login(conn, si);
        return conn;
    }

    public static boolean login(BugzillaConnector conn, ServerInformation si) {
        try {
            login = new LogIn(si.getUsername(), si.getPassword());
            conn.connectTo(si.getUrl());
            conn.executeMethod(login);
            if (!servers.containsKey(si.getId())) {
                servers.put(si.getId(), si);
            }
            return true;
        } catch (ConnectionException | BugzillaException ex) {
            Logger.getLogger(OriusContext.class).error("",ex);
            return false;
        }
    }

    public static String getBugzillaVersion(ServerInformation si){
        BugzillaConnector conn = new BugzillaConnector();
        BugzillaVersion version = new BugzillaVersion();        
        try {            
            conn.connectTo(si.getUrl());
            conn.executeMethod(version);
        } catch (ConnectionException | BugzillaException ex) {
            Logger.getLogger(OriusContext.class).error("", ex);
            return version.getVersion();
        }
        return null;
    }
    public static String getDefaultStyle() {
        return DEFAULT_CSS;
    }

    public static void setDefaultStyle(String styleUrl) {
        OriusContext.DEFAULT_CSS = styleUrl;
    }

    public static AppController loadScreen(String fxml, Stage stage) throws Exception {
        Logger.getLogger(OriusContext.class).debug("No parameters passed to the controller");
        return loadScreen(fxml, stage, null);
    }

    public static AppController loadScreen(String fxml, Stage stage, Map parameters) {

        FXMLLoader loader = new FXMLLoader();
        InputStream in = Orius.class.getResourceAsStream(fxml);
        loader.setBuilderFactory(new JavaFXBuilderFactory());
        loader.setLocation(Orius.class.getResource(fxml));
        AnchorPane page = null;
        try {
            page = (AnchorPane) loader.load(in);
        } catch (IOException ex) {
            Logger.getLogger(OriusContext.class).error("",ex);
        }
        Scene scene = new Scene(page, 800, 600);
        stage.setScene(scene);
        stage.sizeToScene();
        //stage.getScene().getStylesheets().add(OriusContext.class.getResource(DEFAULT_CSS).toExternalForm());
        stage.show();
        stage.getIcons().add(icon);
        AppController controller = (AppController) loader.getController();
        if (parameters != null) {
            controller.setParameters(parameters);
        }
        controller.customInitialiser();
        return controller;

    }

}
