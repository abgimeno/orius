/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package orius.spike;

import com.sun.javafx.collections.NonIterableChange;
import java.util.List;
import javafx.collections.ObservableList;

/**
 *
 * @author agime
 */
public class ParagraphListChange extends NonIterableChange<CharSequence>  {

        private List<CharSequence> removed;

        protected ParagraphListChange(ObservableList<CharSequence> list, int from, int to,
            List<CharSequence> removed) {
            super(from, to, list);

            this.removed = removed;
        }

        @Override
        public List<CharSequence> getRemoved() {
            return removed;
        }

        @Override
        protected int[] getPermutation() {
            return new int[0];
        }
    };
