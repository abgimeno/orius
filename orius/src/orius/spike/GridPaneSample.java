/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package orius.spike;

import euler.bugzilla.beans.ProductBean;
import euler.bugzilla.j2bugzilla.base.Product;
import euler.bugzilla.model.ServerInformation;
import euler.bugzilla.model.query.Fields;
import java.io.IOException;
import javafx.application.Application;
import javafx.stage.Stage;

import javafx.scene.control.ListCell;

import javafx.scene.layout.*;

/**
 * Copyright (c) 2008, 2012 Oracle and/or its affiliates. All rights reserved.
 * Use is subject to license terms.
 */
/**
 * An example of a GridPane layout. There is more than one approach to using a
 * GridPane. First, the code can specify which rows and/or columns should
 * contain the content. Second, the code can alter the constraints of the rows
 * and/or columns themselves, either by specifying the preferred minimum or
 * maximum heights or widths, or by specifying the percentage of the GridPane
 * that belongs to certain rows or columns.
 *
 * @see javafx.scene.layout.GridPane
 * @related controls/text/SimpleLabel
 * @resource icon-48x48.png
 */
public class GridPaneSample extends Application {

    private ServerInformation bc = new ServerInformation("abraham.gimeno@eurodyn.com",
            "agime78!$",
            "http://bugzilla.eurodyn.com/");
   
    private ProductBean ps;
    private String PERSISTENCEUNIT="oriusPU";

    private void init(Stage primaryStage) throws IOException {
        
        /*Parent root = FXMLLoader.load(getClass().getResource("/orius/spike/QueryCreator.fxml"));        
        Scene scene = new Scene(root);        
        primaryStage.setScene(scene);
        primaryStage.show();
        /*
        ps = new ProductService(PERSISTENCEUNIT);
        Group root = new Group();
        final ScrollPane sp = new ScrollPane();
        final HBox hbox = new HBox();
        
        
        
        final Scene scene = new Scene(root, 800, 800);
        sp.setPrefWidth(scene.getWidth());
        sp.setPrefHeight(scene.getHeight());
        scene.widthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number oldSceneWidth, Number newSceneWidth) {
                System.out.println("Width: " + newSceneWidth);
                sp.setPrefWidth(scene.getWidth());
            }
        });
        scene.heightProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number oldSceneHeight, Number newSceneHeight) {
                sp.setPrefHeight(scene.getHeight());
                System.out.println("Height: " + newSceneHeight);
            }
        });
        primaryStage.setScene(scene);

        VBox vbox = new VBox();
        hbox.getChildren().add(vbox);
        
        /* There are some fields, namely version, component and milestone 
         * than can be filtered by the selected products so that
         * values associtated with products the user does not work on 
         * are not displayed
         *//*
        List<Product> storedProd = ps.getActiveProductList();
        for (MyBugSearch.SearchLimiter f : MyBugSearch.SearchLimiter.values()) {
            GetLegalValuesService glv = new GetLegalValuesService(bc, f);
            ListView lv = new ListView();
            lv.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
            lv.itemsProperty().bind(glv.valueProperty());
            glv.start();
            vbox = new VBox();
            vbox.getChildren().add(new Label(f.getName()));
            vbox.getChildren().add(lv);
            hbox.getChildren().add(vbox);
        }
        sp.setContent(hbox);
        primaryStage.getScene().getStylesheets().add(getClass().getResource("main.css").toExternalForm());
        root.getChildren().add(sp);*/

    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        init(primaryStage);
        primaryStage.show();
    }

    private boolean filterResults(Fields f) {
        return(f.getInternalName().equals("component")||
               f.getInternalName().equals("version")||
               f.getInternalName().equals("target_milestone"));
    }

    static class ColorRectCell extends ListCell<Product> {

        @Override
        public void updateItem(Product item, boolean empty) {
            super.updateItem(item, empty);
            if (item != null) {
                setText(item.getName());
            }
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}