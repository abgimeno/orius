/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package orius;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author agime
 */
public class Orius extends Application {
    
    
    private Stage stage;    
            
            
    @Override
    public void start(Stage primaryStage) throws Exception {

        /*Parent root = FXMLLoader.load(getClass().getResource("MainWindow.fxml"));        
        Scene scene = new Scene(root);        
        stage.setScene(scene);
        stage.show();*/        
        stage = primaryStage;      
        stage.initStyle(StageStyle.DECORATED);
        OriusContext.setApplication(this);
        OriusContext.loadScreen(OriusContext.MAINSCREEN, stage);
        
    }

        
    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }


}