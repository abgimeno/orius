/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package orius;

/**
 *
 * @author agime
 */
public enum AppControllers {

        MAINWINDOW(0),
        QUERYCREATOR(1),
        LOGIN(2),         
        PRODUCT(3),
        UPDATE_BUG(4), 
        PRODUCT_SELECTOR(5), 
        COLUMNCONF(6);
        
        
        private final Integer type;

        /**
         * Creates a new {@link SearchLimiter} with the designated name
         *
         * @param name The name Bugzilla expects for this search limiter
         */
        AppControllers(Integer type) {
            this.type = type;
        }

        /**
         * Get the name Bugzilla expects for this search limiter
         *
         * @return A <code>String</code> representing the search limiter
         */
        public Integer getType() {
            return this.type;
        }    
}
