/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package orius;

import euler.fx.controller.AppController;
import orius.conf.product.ProductController;
import euler.bugzilla.beans.BugzillaCredentialsBean;
import euler.bugzilla.builder.BugInfoCellBuilder;
import euler.bugzilla.builder.TableDirector;
import euler.bugzilla.comparator.BugIdComparator;
import euler.bugzilla.comparator.DateComparator;
import euler.bugzilla.comparator.SeverityComparator;
import euler.bugzilla.event.LoadBugCommentsEvent;
import orius.event.RowClickEvent;
import euler.bugzilla.fx.table.BugTableView;
import euler.bugzilla.model.BugInfo;
import euler.bugzilla.model.ServerInformation;
import euler.bugzilla.model.conf.PositionComparator;
import euler.bugzilla.model.conf.QueryTableColumn;
import euler.bugzilla.model.conf.QueryTableConf;
import euler.bugzilla.model.query.Fields;
import euler.bugzilla.utils.EulerExceptionUtils;
import euler.bugzilla.utils.WeakBinder;
import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.ResourceBundle;
import java.util.Map;
import javafx.event.Event;
import javafx.event.EventDispatcher;
import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.util.Callback;
import org.apache.log4j.Logger;
import java.util.List;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.Property;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.SortedList;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.input.InputEvent;
import javax.persistence.NoResultException;
import orius.fx.cell.BugClickableListCell;
import orius.fx.cell.BugClickableStringCell;

/**
 *
 * @author agime
 */
public abstract class BaseController implements AppController<Orius> {

    protected static final String CSS_SUCCESS = "-fx-background-color:radial-gradient(focus-angle 20.0deg, focus-distance 15.0%, center 100.0px 100.0px, radius 300.0px,  derive(lightgreen,0.0%), derive(limegreen , 15.0%));";
    protected static final String CSS_ERROR = "-fx-background-color:radial-gradient(focus-angle 20.0deg, focus-distance 15.0%, center 100.0px 100.0px, radius 300.0px,  derive(tomato,0.0%), derive(red , 15.0%)) ;";

    protected final WeakBinder binder = new WeakBinder();
    protected static final String PERSISTENCEUNIT = "oriusPU";
    protected Orius app;
    protected OriusContext context;

    protected static ServerInformation serverCredentials;

    protected Map parameters;
    protected TableDirector director = new TableDirector();   
    protected EventDispatcher oldDispatcher;

    @FXML
    protected AnchorPane root;
    protected EulerExceptionUtils guiLog = EulerExceptionUtils.getInstance();
    protected static final Logger log = EulerExceptionUtils.LoggerFactory.make();

    public BaseController() {

    }

    @Override
    public void setParameters(Map parameters) {
        this.parameters = parameters;
    }

    //protected abstract void initialiseSuper();
    protected abstract void setController(); //add controller to context

    /*
     * sets "serverCredentials" variable from credentials retrieved from DB and
     * sets "conn" bugzilla connector using those credentials
     */
    protected void setCredentials() throws Exception {
        try {
            BugzillaCredentialsBean bcs = new BugzillaCredentialsBean(PERSISTENCEUNIT);
            serverCredentials = bcs.getCredentials();
        } catch (NoResultException ex) {
            log.info("No credentials found on database");
        } catch (IOException ex) {
            guiLog.handleException(ProductController.class, ex);
        }

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            log.info("Initialising base controller");
            setCredentials();
            setController();
        } catch (Exception ex) {
            guiLog.handleException(ProductController.class, ex);
        }
    }


    /* the below are the parameters filtered by product on bugzilla, 
     * since user selects a subset of product values not associated with the 
     * products selected by the user are left out
     */

    protected boolean filterResults(Fields f) {
        return (f.getInternalName().equals("component")
                || f.getInternalName().equals("version")
                || f.getInternalName().equals("target_milestone"));
    }

    protected boolean isSecondary(Fields f) {
        return (f.getInternalName().equals("component")
                || f.getInternalName().equals("version")
                || f.getInternalName().equals("target_milestone")
                || f.getInternalName().equals("op_sys")
                || f.getInternalName().equals("rep_platform"));
    }

    @Override
    public void setApp(Orius application) {
        this.app = application;
        OriusContext.setApplication(app);
    }

    @Override
    public Orius getApp() {
        return app;
    }

}
